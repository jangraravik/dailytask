<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('task_modl');
		$this->load->model('user_modl');
		$this->task_modl->makeArchive();
		$this->output->enable_profiler(FALSE);
		if(!$this->session->userdata('userLogedIn')){redirect('/user/login','refresh');}	  
	}
	
	public function index()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('task',$data);
	}


	public function add()
	{
		if($this->input->post('action') === 'Add'){
			//test($_POST);exit;
			$dataInsert = array(
			'usr_id' => $this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'],
			'tsk_title' => escStr($this->input->post('valTaskTtl')),
			'tsk_desc' => escStr($this->input->post('valTaskDes')),
			'tsk_strt_date_time' => strtotime($this->input->post('valTskDate').$this->input->post('valTskTim')),
			'tsk_rept_typ' => $this->input->post('valTskRepTyp'),
			'tsk_rept_tim' => $this->input->post('valTskReptTim'),
			'color_theme' => $this->input->post('valTskClrThem'),
			'tsk_added_on' => time(),
			'tsk_status' => 1
			);
			//test($dataInsert);exit;
			$this->task_modl->create($dataInsert);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'A new Task is Added';
			$this->session->set_flashdata($dataFlash);
			redirect('task','refresh');
		}
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$this->load->view('task_add_edit',$data);
	}
	
	public function edit()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$data['doid'] = decQryStr($this->uri->segment(3));

		if($this->input->post('action') === 'Save'){
		//test($_POST);
			$dataUpdate = array(
			'usr_id' => $this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'],
			'tsk_title' => $this->input->post('valTaskTtl'),
			'tsk_desc' => $this->input->post('valTaskDes'),
			'tsk_strt_date_time' => strtotime($this->input->post('valTskDate').$this->input->post('valTskTim')),
			'tsk_rept_typ' => $this->input->post('valTskRepTyp'),
			'tsk_rept_tim' => $this->input->post('valTskReptTim'),
			'color_theme' => $this->input->post('valTskClrThem'),
			'tsk_status' => $this->input->post('valStatus')
			);
			if($this->input->post('valStatus') == 2){
				$dataUpdate['color_theme'] = 0;
			}
			//test($dataUpdate);exit;
			$this->task_modl->update($dataUpdate,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Task Details are Updated';
			$this->session->set_flashdata($dataFlash);
			//redirect($this->agent->referrer(),'refresh');			
				/* Check Status and Take to the Page */
				if($dataUpdate['tsk_status'] == 0){
					redirect('/task/archive','refresh');
				}
				if($dataUpdate['tsk_status'] == 1){
					redirect('/task','refresh');
				}
				if($dataUpdate['tsk_status'] == 2){
					redirect('/task/completed','refresh');
				}
				if($dataUpdate['tsk_status'] == 3){
					redirect('/task/cancelled','refresh');
				}
		}
		$data['action'] = 'edit';
		$this->load->view('task_add_edit',$data);
	}


	public function detail()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['doid'] = decQryStr($this->uri->segment(3));
		$this->load->view('task_detail',$data);
	}
	
	public function archive()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('task_archive',$data);
	}
	
	public function cancelled()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['doid'] = decQryStr($this->uri->segment(3));
		if(isset($data['doid']) && $data['doid']){
			$dataUpdate = array(
			'tsk_status' => 3,
			);
			$this->task_modl->update($dataUpdate,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Task Status is set to Cancelled';
			$this->session->set_flashdata($dataFlash);
			redirect($this->agent->referrer(),'refresh');
		}
		
		$this->load->view('task_cancelled',$data);
	}
	
	public function completed()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$data['doid'] = decQryStr($this->uri->segment(3));
		if(isset($data['doid']) && $data['doid']){
			$dataUpdate = array(
			'tsk_status' => 2,
			'color_theme' => 0
			);
			$this->task_modl->update($dataUpdate,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Task Status is set to Compleated';
			$this->session->set_flashdata($dataFlash);
			redirect($this->agent->referrer(),'refresh');
		}
		$this->load->view('task_completed',$data);
	}

	public function delete()
	{
		//$data['pagTab'] = $this->uri->segment(2);
		//echo $data['action'] = $this->uri->segment(2);
		//echo $this->agent->referrer();exit;
		$data['doid'] = decQryStr($this->uri->segment(3));
		$this->task_modl->detete($data['doid']);
		$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
		$dataFlash['MSGS_VALUE'] = 'Task is Deleted Successfully';
		$this->session->set_flashdata($dataFlash);
		
		redirect($this->agent->referrer(),'refresh');		
	}

}
