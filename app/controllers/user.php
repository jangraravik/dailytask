<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {




	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_modl');
		$this->output->enable_profiler(FALSE);
		/* Because This is Login Page */
		//if(!$this->session->userdata('userLogedIn')){redirect('/user/login','refresh');}		  
	}
	
	
	
	
	
	
	
	public function index()
	{
		//$this->login_account();
	}
	
	
	
	
	
	

	
	
	public function login_register()
	{
		
		if($this->input->post('action') === 'Register'){
			/* A default image */
				$dataImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAuCAMAAABgZ9sFAAAAWlBMVEU0btx0nuxchuSUuvxMfuSEqvRkkuxEduSkxvxEcuSMsvREctycwvxsmuw8ctx8pvRkjuxUguSErvQ8btx8ovRkiuycvvxUfuRskuykyvyMsvykwvx0muyMrvTdpLppAAABlklEQVRIidWU246DMAxELQEbW0AocohUoP//m2tuJXFoujwuQQhVw2EyeApTc2cBNjfWBLfgt+m34P/AO67HJT1F4NQ9hv7Ryd0f6OiA2FJN7NIXJHTsgWpTG0uWes1P6M+BTb0v0T+/JONgUy5X4Xeo6Mq4oV27Xq1BRY8expHt28vC5zHjXbyc5E3vMEdv6dSubtosfaYYz3OWXgUxrlFWWbqHwI0s8GE0mt7gEDmnYfntMx09h8Gwz+YuD7zAvtkQp57MjLDkSx3p2Bc22YnczpaYGWhuFiff6HJiMb4K3MSR9Qv6tCEP8Td6eEy6T5qOzeh+hr4sy+FnHj3qeY//AboBWJKRplorG4Zy9sFfQtwmGTDRLi2SGNcsiYgfHgP6iS4MWRN3qbbysbjC1PtUUFSkoFJQYeIdhVOH4PfkGAKfzMzIdeQkdCQVVPSjpbF2X1JBTe8kk1MRTz3Nn+iX27XtpOmOQkW43Ut6x0kkOe/Oag95OlmjMz/u+ILO2sL5moS+BKl3GIxBm7TJXYxMYGan/wK6CG3ByPEYXQAAAABJRU5ErkJggg==";
			//test($_POST);exit;
			$dataInsert = array(
			'usr_name' => $this->input->post('valName'),
			'usr_email' => $this->input->post('valEmail'),
			'usr_pass' => $this->input->post('valPass'),
			'usr_photo' => $dataImage,
			'usr_doj' => time(),
			'usr_role' => 2,
			'usr_isactive' => 0
			);
			$this->user_modl->register($dataInsert);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'You are Registered Successfully, Waiting for Admin Approval';
			$this->session->set_flashdata($dataFlash);
			redirect('/user/login','refresh');
		}
		
		$this->load->view('account_register');
	}
	
	
	
	
	
	
	
	public function login_account()
	{
		if($this->input->post('action') === 'Login'){
			//print_r($_POST);exit;

			$isLoged = $this->user_modl->validate_user($this->input->post('valEmail'),$this->input->post('valPass'));
			//test($this->session->all_userdata());exit;

			if($isLoged){
				$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Your are Logged in Successfully';
				$this->session->set_flashdata($dataFlash);
				redirect('/dashboard','refresh');
			}else{
				$dataFlash['MSGS_TYPE'] = 'error'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Invalid Login Details';
				$this->session->set_flashdata($dataFlash);
				redirect('/user/login','refresh');				
			}
		}
		$this->load->view('account_login');
	}








	public function logout_account()
	{
		$dataUpdate = array(
		   'usr_lastlogin'=> time()
		);
		$this->user_modl->update($dataUpdate,$this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID']);
		//$this->session->sess_destroy();
		$this->session->unset_userdata('userLogedIn');
		$this->session->unset_userdata('dataUser');			
		$dataFlash['MSGS_TYPE'] = 'info'; /* success|info|error|alert */
		$dataFlash['MSGS_VALUE'] = 'Your are Logged Out Successfully';
		$this->session->set_flashdata($dataFlash);
		redirect('/user/login','refresh');
	}
	
	
	
	
	
	
	
	
	
	public function reset_password()
	{
		if($this->input->post('action') === 'Submit'){
			//print_r($_POST);exit;
			/* Send Email With Verification Code */
			$isValid = $this->user_modl->validate_user_email($this->input->post('valEmail'));
			if($isValid){
				$dataFlash['MSGS_TYPE'] = 'info'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Please check your Email for Verification Code';
				$this->session->set_flashdata($dataFlash);
				redirect('/user/verify','refresh');			
			}else{
				$dataFlash['MSGS_TYPE'] = 'error'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Invalid Email Address';
				$this->session->set_flashdata($dataFlash);
			}
		}
		$this->load->view('account_reset');
	}

	






	public function verify_account()
	{
		if($this->input->post('action') === 'Verify'){
			//print_r($_POST);exit;
			/* Verify Email With Verification Code */
			$isValid = $this->user_modl->validate_user_email_verify_code($this->input->post('valEmail'),$this->input->post('valVerifyCode'));
			if($isValid){
				$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Validated, You can Reset New Password';
				$this->session->set_flashdata($dataFlash);
				$this->session->set_userdata('vCodeID',$this->input->post('valVerifyCode'));
				redirect('/user/reset-password','refresh');			
			}else{
				$dataFlash['MSGS_TYPE'] = 'error'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Invalid Email Address or Verification Code';
				$this->session->set_flashdata($dataFlash);
			}
		}		
		
		$this->load->view('account_verify');
	}
	
	
	
	
	
	
	
	
	public function reset_new_password()
	{
		if($this->input->post('action') === 'Save'){
			//print_r($_POST);exit;
			$resetPassUser = array(
			   'usr_pass'=> $this->input->post('valPass1'),
			   'usr_verify_code'=> 0
			);
			$vCodeID = $this->session->userdata('vCodeID');
			$isReset = $this->user_modl->set_new_password($resetPassUser,$vCodeID);
			if($isReset){
				$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Password is Reset Successfully, you can Login now';
				$this->session->set_flashdata($dataFlash);
				redirect('/user/login','refresh');	
			}
		}
		$this->load->view('account_reset_new_password');
	}
	
	
	
	
	

	
	
	public function profile_account()
	{
		if($this->input->post('action') === 'Save'){
			//test($_POST);exit;
			if(!empty($_FILES['valPhoto']['tmp_name']) && ($_FILES['valPhoto']['error'] == 0) && ($_FILES['valPhoto']['size'] <= 200000)){ /* Should be <= 100 KB */
				$dataImage = 'data: '.$_FILES['valPhoto']['type'].';base64,'.base64_encode(file_get_contents($_FILES['valPhoto']['tmp_name']));			
			}
			
			$dataUpdate = array(
			'usr_name' => $this->input->post('valName'),
			//'usr_email' => $this->input->post('valEmail'),
			'usr_mobile' => $this->input->post('valMobile'),
			'usr_dob' => strtotime($this->input->post('valDob')),
			//'usr_photo' => $dataImage,
			'usr_gender' => $this->input->post('valGender'),
			'usr_about' => $this->input->post('valAbout'),
			'usr_place_city' => $this->input->post('valCityPlace')
			);
			
			if(!empty($_FILES['valPhoto']['tmp_name']) && ($_FILES['valPhoto']['error'] == 0) && ($_FILES['valPhoto']['size'] <= 200000)){ /* Should be <= 100 KB */
			$dataUpdate['usr_photo'] = $dataImage;
			}

			//test($dataUpdate);exit;
			$this->user_modl->update($dataUpdate,$this->session->userdata['dataUser']['usr_id']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Profile details are Updated';
			$this->session->set_flashdata($dataFlash);
		}
		$data['pagTab'] = 'dashboard';
		$this->load->view('account_profile',$data);
	}
	
	
	
	
	
	
	public function profile_settings()
	{
		$data['uid'] = $this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'];	
		if($this->input->post('action') === 'Save'){
			//test($_POST);exit;
			$isValid = $this->user_modl->validate_user_password($this->input->post('valPassCurrent'),$data['uid']);
			if($isValid){
				$dataUpdate = array(
				'usr_pass' => $this->input->post('valPassConfirm')
				);

				$this->user_modl->update($dataUpdate,$data['uid']);
				$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Profile Password is Changed';
				$this->session->set_flashdata($dataFlash);
			}else{
				$dataFlash['MSGS_TYPE'] = 'error'; /* success|info|error|alert */
				$dataFlash['MSGS_VALUE'] = 'Invalid Current Profile Password, Try again';
				$this->session->set_flashdata($dataFlash);
			}
			//test($dataUpdate);exit;
		}
		$data['pagTab'] = 'dashboard';
		$this->load->view('account_settings',$data);
	}





}
