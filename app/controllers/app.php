<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_modl');
		$this->load->model('task_modl');
		$this->load->model('note_modl');
		$this->load->model('wallet_modl');				
		$this->output->enable_profiler(FALSE);		
	}
	
	public function index()
	{
		//echo 'Welcome, to User Register and Login Tutorial<br><br><a href="user/login">Login</a><br><a href="user/register">Register</a>';
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('index',$data);		
	}


	public function dashboard()
	{
		if(!$this->session->userdata('userLogedIn')){redirect('/user/login','refresh');}
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('dashboard',$data);
	}
	
	

	public function calendar()
	{
		if(!$this->session->userdata('userLogedIn')){redirect('/user/login','refresh');}
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('calendar',$data);
	}

}
