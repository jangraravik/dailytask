<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wallet extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('wallet_modl');
		$this->load->model('user_modl');		
		$this->output->enable_profiler(FALSE);
		if(!$this->session->userdata('userLogedIn')){redirect('/user/login','refresh');}		  
	}
	
	public function index()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('wallet',$data);
	}


	public function add()
	{
		if($this->input->post('action') === 'Add'){
			//test($_POST);exit;
			$dataInsert = array(
			'usr_id' => $this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'],
			'wlt_trns_title' => $this->input->post('valWltTtl'),
			'wlt_trns_desc' => $this->input->post('valWltDes'),
			'wlt_trns_date' => strtotime($this->input->post('valWltDate').$this->input->post('valWltTim')),
			'wlt_trns_type' => $this->input->post('valWltTranType'),
			'wlt_trns_amount' => $this->input->post('valWltTranAmount'),
			'wlt_trns_status' => 1,
			'wlt_trns_added_on' => time()
			);
			//test($dataInsert);exit;
			$this->wallet_modl->create($dataInsert);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'A new Transaction is Added';
			$this->session->set_flashdata($dataFlash);
			redirect('wallet','refresh');	
		}
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$this->load->view('wallet_add_edit',$data);
	}
	
	public function edit()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$data['doid'] = decQryStr($this->uri->segment(3));

		if($this->input->post('action') === 'Save'){
		//test($_POST);exit;
			$dataUpdate = array(
			'wlt_trns_title' => $this->input->post('valWltTtl'),
			'wlt_trns_desc' => $this->input->post('valWltDes'),
			'wlt_trns_date' => strtotime($this->input->post('valWltDate').$this->input->post('valWltTim')),
			'wlt_trns_type' => $this->input->post('valWltTranType'),
			'wlt_trns_amount' => $this->input->post('valWltTranAmount')
			);
			//test($dataUpdate);exit;
			$this->wallet_modl->update($dataUpdate,$this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'],$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Transaction details are Updated';
			$this->session->set_flashdata($dataFlash);
			//redirect($this->agent->referrer(),'refresh');
			redirect('wallet','refresh');
		}
		$data['action'] = 'edit';
		$this->load->view('wallet_add_edit',$data);
	}

}
