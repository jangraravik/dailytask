<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Note extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('note_modl');
		$this->load->model('user_modl');
		$this->output->enable_profiler(FALSE);
		if(!$this->session->userdata('userLogedIn')){redirect('/user/login','refresh');}	  
	}
	
	public function index()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$this->load->view('note',$data);
	}


	public function add()
	{
		if($this->input->post('action') === 'Add'){
			//test($_POST);exit;
			$dataInsert = array(
			'usr_id' => $this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'],
			'not_title' => $this->input->post('valNotTtl'),
			'not_desc' => $this->input->post('valNotDes'),
			'not_added_on' => time(),
			'not_status' => 1
			);
			
			$this->note_modl->create($dataInsert);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'A new Note is Added';
			$this->session->set_flashdata($dataFlash);
			redirect('note','refresh');
			
		}
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$this->load->view('note_add_edit',$data);
	}

	public function edit()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$data['doid'] = decQryStr($this->uri->segment(3));

		if($this->input->post('action') === 'Save'){
		//test($_POST);
			$dataUpdate = array(
			'usr_id' => $this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID'],
			'not_title' => $this->input->post('valNotTtl'),
			'not_desc' => $this->input->post('valNotDes'),
			'not_status' => $this->input->post('valStatus')
			);

			//test($dataUpdate);exit;
			$this->note_modl->update($dataUpdate,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Task Details are Updated';
			$this->session->set_flashdata($dataFlash);
			redirect('note','refresh');
		}
		$data['action'] = 'edit';
		$this->load->view('note_add_edit',$data);
	}
	
	public function detail()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['doid'] = decQryStr($this->uri->segment(3));
		$this->load->view('note_detail',$data);
	}
	

	
	public function completed()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$data['doid'] = decQryStr($this->uri->segment(3));
		if(isset($data['doid']) && $data['doid'] && $data['action'] == 'completed'){
			//echo $data['doid'];exit;
			$dataUpdate = array(
			'not_status' => 2
			);
			$this->note_modl->update($dataUpdate,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Note Status is set to Compleated';
			$this->session->set_flashdata($dataFlash);
			redirect($this->agent->referrer(),'refresh');
		}else{
			$this->load->view('note_completed',$data);
		}
	}
	
	public function cancelled()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(2);
		$data['doid'] = decQryStr($this->uri->segment(3));
		if(isset($data['doid']) && $data['doid'] && $data['action'] == 'cancelled'){
			$dataUpdate = array(
			'not_status' => 3,
			);
			$this->note_modl->update($dataUpdate,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Note Status is set to Cancelled';
			$this->session->set_flashdata($dataFlash);
			redirect($this->agent->referrer(),'refresh');
		}
		
		$this->load->view('note_cancelled',$data);
	}
	
	public function deleted()
	{
		//$data['pagTab'] = $this->uri->segment(2);
		//echo $data['action'] = $this->uri->segment(2);
		//echo $this->agent->referrer();exit;
		$data['doid'] = decQryStr($this->uri->segment(3));
		$this->note_modl->detete($data['doid']);
		$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
		$dataFlash['MSGS_VALUE'] = 'Note is Deleted Successfully';
		$this->session->set_flashdata($dataFlash);
		
		redirect($this->agent->referrer(),'refresh');		
	}
	
	public function timeline()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['doid'] = decQryStr($this->uri->segment(3));
		$this->load->view('note_timeline',$data);
	}
	
	public function extend()
	{
		$data['pagTab'] = $this->uri->segment(1);
		$data['action'] = $this->uri->segment(3);
		$data['noteid'] = decQryStr($this->uri->segment(4));
		$data['doid'] = decQryStr($this->uri->segment(5));
	
		if($this->input->post('action') === 'Add'){
		//test($_POST);exit;
			$dataInsert = array(
			'not_id' => $data['noteid'],
			'not_timlin_comnt' => $this->input->post('valNotDes'),
			'not_timlin_datetime' => strtotime($this->input->post('valNotDate').$this->input->post('valNotTim'))
			);
			//test($dataInsert);exit;
			$this->note_modl->create_timeline($dataInsert);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Note is Extended';
			$this->session->set_flashdata($dataFlash);
			redirect('note/timeline/'.encQryStr($data['noteid']),'refresh');
		}
		
		if($this->input->post('action') === 'Save'){
		//test($_POST);exit;
			$dataInsert = array(
			'not_timlin_comnt' => $this->input->post('valNotDes'),
			'not_timlin_datetime' => strtotime($this->input->post('valNotDate').$this->input->post('valNotTim'))
			);
			//test($dataInsert);exit;
			$this->note_modl->update_timeline($dataInsert,$data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Note is Updated';
			$this->session->set_flashdata($dataFlash);
			redirect('note/timeline/'.encQryStr($data['noteid']),'refresh');
		}
		
		if(isset($data['action']) && $data['action'] === 'delete'){
//			echo $data['doid'];exit;
			$this->note_modl->detete_timeline($data['doid']);
			$dataFlash['MSGS_TYPE'] = 'success'; /* success|info|error|alert */
			$dataFlash['MSGS_VALUE'] = 'Note is Deleted Successfully';
			$this->session->set_flashdata($dataFlash);
			redirect('note/timeline/'.encQryStr($data['noteid']),'refresh');
		}


		$this->load->view('note_extend',$data);
	}	

	
}