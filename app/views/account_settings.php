<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>
Settings
</title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">

<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>

<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3> <i class="glyphicon-settings"></i> <?php echo $this->user_modl->getUsrName($this->session->userdata['dataUser']['usr_id']);?>' Settings</h3>
		<p align="right"><button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button></p>
	</div>
<p></p>
<?php include('inc_msgs.php');?> 

<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb">
    
    <div class="control-group">
        <label for="valPassCurrent" class="control-label">Current Password</label>
        <div class="controls">
            <input type="text" name="valPassCurrent" id="valPassCurrent" data-rule-required="true" class="input-xlarge" value="">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valPassNew" class="control-label">New Password</label>
        <div class="controls">
            <input type="text" name="valPassNew" id="valPassNew" data-rule-required="true" class="input-xlarge" value="">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valPassConfirm" class="control-label">Confirm Password</label>
        <div class="controls">
            <input type="text" name="valPassConfirm" id="valPassConfirm" data-rule-required="true" data-rule-equalTo="#valPassNew" class="input-xlarge" value="">
        </div>
    </div>
    
    <div class="form-actions">
        <button type="submit" name="action" value="Save" class="btn btn-primary"><i class="icon-save"></i> Save</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>