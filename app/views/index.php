<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="dailyTask">
<meta name="author" content="dailyTask">
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>dailyTask</title>
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/stylish-portfolio.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/blur.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/font-awesome.css" type="text/css">

<script type="text/javascript" src="<?php echo base_url('script');?>/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('script');?>/jquery.blur.min.js"></script>
<script type="text/javascript">
$(window).load(function(){		
        $('.portfolio-item img').picanim({initEf:'blur',hoverEf:'fadeIn'});
});
</script>
</head>

<body>

    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <a href="#top"  onclick = $("#menu-close").click(); >Manual</a>
            </li>
            <li>
                <a href="#about" onclick = $("#menu-close").click(); >dailyTask</a>
            </li>
            <li>
                <a href="#services" onclick = $("#menu-close").click(); >What it can Do!</a>
            </li>
            <li>
                <a href="#portfolio" onclick = $("#menu-close").click(); >Let's take a walk</a>
            </li>
        </ul>
    </nav>

    <!-- Header -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <img src="<?php echo base_url('image');?>/logo@2x.png"><h1>dailyTask</h1>
            <h3>Manage Your Daily Life</h3>
        </div>
    </header>

    <!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>A smart way to manage your daily schedule</h2>
                    <p class="lead">dailyTask is a SaaS mobile/web application with a vision to manage somebody's busy and tentative schedule, delivering cutting edge applications that serve best in categories. All with a very user friendly environment.</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Services -->
    <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
    <section id="services" class="services bg-primary">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>What it can Do!</h2>
                    <hr class="big">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-bicycle fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Task</strong>
                                </h4>
                                <p>Schedule all your daily tasks with date and time reminder</p>
                                <!-- <a href="#" class="btn btn-light">Learn More</a>-->
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-tasks fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Note</strong>
                                </h4>
                                <p>Keep notes of all your deals and activity with log</p>
                                <!-- <a href="#" class="btn btn-light">Learn More</a>-->
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-calendar fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Calendar</strong>
                                </h4>
                                <p>Filter all your tasks and warns you before it happens</p>
                                <!-- <a href="#" class="btn btn-light">Learn More</a>-->
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-money fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Wallet</strong>
                                </h4>
                                <p>All your Wallet Transactions record you spend or received.</p>
                                <!-- <a href="#" class="btn btn-light">Learn More</a>-->
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Callout -->
    <aside class="callout">
        <div class="text-vertical-center">
            <!--<h1>Never miss anything</h1>-->
        </div>
    </aside>

    <!-- Portfolio -->
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2>Let's take a walk</h2>
                    <hr class="small">
                    <div class="row" id="takeawalk">
                        <div class="col-md-6">
                            <div class="portfolio-item test cf">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour1.png">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour2.png">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour3.png">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour4.png">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="takeawalk">
                        <div class="col-md-6">
                            <div class="portfolio-item test cf">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour5.png">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour6.png">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour7.png">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="<?php echo base_url('image');?>/dayPlaner-task-tour8.png">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Call to Action -->
    <aside class="call-to-action bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>Start managing you day</h3>
                    <a href="/user/login" class="btn btn-lg btn-light">Login Account</a>
                    <a href="/user/register" class="btn btn-lg btn-dark">Create Account</a>
                </div>
            </div>
        </div>
    </aside>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
				<p><br><br><br><br></p>
                    <h4><strong>Got a Question?</strong></h4>
                    
                    <ul class="list-unstyled">
                        <li><i class="fa fa-phone fa-fw"></i> +91 9811 66 9942</li>
                        <li><i class="fa fa-envelope-o fa-fw"></i> info[at]dailytask.in
                        </li>
                    </ul>
                    <br>
<!--
                    <ul class="list-inline">
                        <li>
                            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>

                    <hr class="small">
                    <p class="text-muted">Copyright &copy; dailyTask 2015</p>
-->
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    </script>
</body>
</html>