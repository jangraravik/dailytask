<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
//test($this->session->userdata['dataUser']);
$dataUsr = $this->user_modl->user_info_all($this->session->userdata['dataUser']['usr_id']);
//test($dataUsr);exit;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Profile</title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">
<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>
</head>
<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="box">
          <div class="box-title">
            <h3> <i class="icon-user"></i> Hello, <?php echo $this->user_modl->getUsrName($this->session->userdata['dataUser']['usr_id']);?></h3>
            <div class="pull-right">
						<ul class="stats">
							<li>
								<div class="details">
								You Last loged in <?php //echo timeElasped($this->session->userdata['dataUser']['usr_lastlogin']); ?>
								<?php echo timeElasped($this->session->userdata['dataUser']['usr_lastlogin']); ?>
								</div>
							</li>
						</ul>
					</div>
          </div>
					
          <p></p>
<?php include('inc_msgs.php');?>       

<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb" enctype="multipart/form-data">
    
    <div class="control-group">
        <label for="valName" class="control-label">Full Name</label>
        <div class="controls">
            <input type="text" name="valName" id="valName" class="input-xlarge" value="<?php echo $dataUsr['usr_name']; ?>" data-rule-required="true">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valAbout" class="control-label">About Yourself</label>
        <div class="controls">
            <textarea name="valAbout" id="valAbout" data-rule-required="true" class="input-xlarge"><?php echo $dataUsr['usr_about']; ?></textarea>
        </div>
    </div>
    
    <div class="control-group">
    <label for="gender" class="control-label">Gender</label>
    <div class="controls">
        <div class="check-demo-col">
            <div class="check-line">
                <input type="radio" id="valGenderM" class='icheck-me' name="valGender" data-skin="minimal" value="m" <?php echo ($dataUsr['usr_gender'] == 'm') ? 'checked':NULL; ?>>
                <label class='inline' for="valGenderM">Male</label>
            </div>
            <div class="check-line">
                <input type="radio" id="valGenderF" class='icheck-me' name="valGender" data-skin="minimal" value="f" <?php echo ($dataUsr['usr_gender'] == 'f') ? 'checked':NULL; ?> >
                <label class='inline' for="valGenderF">Female</label>
            </div>
        </div>
    </div>
    </div>
    
    <div class="control-group">
        <label for="valDob" class="control-label">Date of Birth</label>
        <div class="controls">
            <input type="text" name="valDob" id="valDob" class="input-xlarge datepick" readonly data-rule-required="true" value="<?php echo date("d M Y",$dataUsr['usr_dob']); ?>">
<script type="text/javascript">
$('#valDob').datepicker({
//	format: 'dd-mm-yyyy',
//	format: 'dd/mm/yyyy',
	format: 'dd M yyyy',
//	startDate: date,
	endDate: '+0d',
	autoclose: true
});
</script>                                            
        </div>
    </div>
    
    <div class="control-group">
        <label for="valCityPlace" class="control-label">Place/City</label>
        <div class="controls">
            <input type="text" name="valCityPlace" id="valCityPlace" value="<?php echo $dataUsr['usr_place_city']; ?>" class="input-xlarge" data-rule-required="true">
        </div>
    </div>

    <div class="control-group">
        <label for="valMobile" class="control-label">Mobile Number</label>
        <div class="controls">
            <input type="text" name="valMobile" id="valMobile" value="<?php echo $dataUsr['usr_mobile']; ?>" class="input-xlarge" data-rule-number="true" data-rule-required="true">
        </div>
    </div>
    <div class="control-group">
        <label for="valEmail" class="control-label">Email Address</label>
        <div class="controls">
            <input type="text" name="valEmail" id="valEmail" disabled value="<?php echo $dataUsr['usr_email']; ?>" class="input-xlarge" data-rule-email="true" data-rule-required="true">
        </div>
    </div>
    
    
<div class="control-group">
    <label for="textfield" class="control-label">Profile Thumb</label>
    <div class="controls">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail"><img src="<?php echo $this->user_modl->getUsrPhoto($this->session->userdata['dataUser']['usr_id']); ?>" /></div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name='valPhoto' /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
    </div>
</div>
    

    <div class="form-actions">
        <button type="submit" name="action" value="Save" class="btn btn-primary"><i class="icon-save"></i> Save</button>
        <button type="button" class="btn btn-small" onClick="history.back(-1);">Cancel</button>
    </div>
    </form>
</div>
          
        </div>
      </div>
    </div>
    
  </div>
</div>
</body>
</html>
