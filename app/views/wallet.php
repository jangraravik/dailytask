<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
## Finding the Last and First Day of the Given Month ##
if(!isset($_GET['month']) || empty($_GET['month'])){
	$mnthStart = strtotime("first day of this month 12:00:00 AM");
	$mnthEnd = strtotime("last day of this month 11:59:00 PM");
}else{
	list($mnthStart,$mnthEnd) = anyMonth(date("M Y",$_GET['month']));
}

$dataUsrTrns = $this->wallet_modl->getAllWltTranByUser($this->session->userdata['dataUser']['usr_id'],$mnthStart,$mnthEnd);
//test($dataUsrTrns);exit;


$total = array();
$total['received'] = 0;
$total['paid'] = 0;
$total['lend'] = 0;
$total['lend Received'] = 0;
$total['borrowed'] = 0;
$total['borrowed Paid'] = 0;

foreach($dataUsrTrns as $catgTotal){
	switch($catgTotal['wlt_trns_type']){
		case "received":
		$total['received']  += round($catgTotal['wlt_trns_amount'],2);
		break;
		
		case "paid":
		$total['paid']  += round($catgTotal['wlt_trns_amount'],2);
		break;
		
		case "lend":
		$total['lend']  += $catgTotal['wlt_trns_amount'];
		break;
		
		case "lend Received":
		$total['lend Received']  += $catgTotal['wlt_trns_amount'];
		break;		
		
		case "borrowed":
		$total['borrowed']  += $catgTotal['wlt_trns_amount'];
		break ;
		
		case "borrowed Paid":
		$total['borrowed Paid']  += $catgTotal['wlt_trns_amount'];
		break ;		
	}
}

//test($dataUsrTrns);exit;

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Wallet Transactions</title>

<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php //echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">


<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.core.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/bootbox/jquery.bootbox.js"></script>
<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script src="<?php echo base_url('script');?>/application.js"></script>
<script src="<?php echo base_url('script');?>/demonstration.js"></script>
<script src="<?php echo base_url('script');?>/jquery.quicksearch.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#id_search").quicksearch("table tbody tr", {
			noResults: '#noresults',
			stripeRows: ['odd', 'even'],
			loader: 'span.loading'
		});
	});
</script>

</head>
<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<i class="glyphicon-wallet"></i>
			Wallet Transactions          
		</h3>
        <p align="right"><button class="btn btn-primary" name="action" value="add" onClick="window.open('/wallet/add','_self');"><i class="icon-plus"></i> Add</button></p>
	</div>
    <p></p>
	<?php include('inc_msgs.php');?> 
</div>
</div>
</div>

<div class="row-fluid">
	<div class="span12">
        <div class="box">    
                <div class="row-fluid">
                    <div class="span4 text-left"><button class="btn btn-success" onClick="window.open('?month=<?php echo (isset($_GET['month'])? strtotime(date("Y-m-d", $_GET['month'])." - 1 month"): strtotime(" -1 month")); ?>','_self');"><i class="icon-chevron-left"></i></button></div>
                    <div class="span4 text-center"><h4>
                    <?php echo (!isset($_GET['month']) ? date("F, Y") : date("F, Y",$_GET['month'])); ?></h4></div>
                    <div class="span4 text-right"><button class="btn btn-success" onClick="window.open('?month=<?php echo (isset($_GET['month'])? strtotime(date("Y-m-d", $_GET['month'])." + 1 month"): strtotime(" +1 month")); ?>','_self');"><i class="icon-chevron-right"></i></button></div>
                </div>
        </div>
    </div>
</div>

<div class="row-fluid">
	<div class="span12">
        <div class="box">
            <div class="box-content">
	<form action="#">
			<fieldset>
				<input type="text" placeholder="Search" class="input-block-level" name="search" value="" id="id_search" /> <span class="loading">Searching...</span>
			</fieldset>
	</form>
<table id="matrix" class="table table-striped table-invoice">
                    <thead>
                        <tr>
                            <th width="60%">Transaction</th>
                            <th width="20%" class='hidden-480'>Time</th>
                            <th width="10%">Type</th>
                            <th width="10%" colspan="2" class='tr total'>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr id="noresults">
                    <td colspan="4">No match found!</td>
                    </tr>
<?php foreach($dataUsrTrns as $dataTrn){ ?>
                        <tr>
                            <td class='name'>
							<a href="/wallet/edit/<?php echo encQryStr($dataTrn['wlt_trns_id']); ?>"><?php echo $dataTrn['wlt_trns_title']; ?></a>
								<span class="task-actions"><a href="?action=delete&amp;doid=205" class='task-delete' rel="tooltip" title="Delete"><i class="icon-remove"></i></a></span>
							</td>
                            <td class='hidden-480'>
							<div id="txtRoll"><span id="txtElasped"><?php echo timeElasped($dataTrn['wlt_trns_date']); ?></span> <span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataTrn['wlt_trns_date']); ?></span></div>
                            </td>
                            <td class='trantype'><?php echo ucfirst($dataTrn['wlt_trns_type']); ?></td>
                            <td colspan="2" class='total'>
							<?php 
							echo formatMoney($dataTrn['wlt_trns_amount']);
							//$dataTrn['wlt_trns_amount']; ?>
                            </td>
                        </tr>
<?php } ?>

</tbody>
</table>

    

            </div>
        </div>
    </div>
</div>



<div class="row-fluid">
	<div class="span12">
        <div class="box">    
                <div class="row-fluid">                  
                    <div class="span12">
                    <table>
<tbody>                
        <tr>
          <td colspan="3" style="text-align:right"><span class="light">Received</span></td>
          <td width="10" style="text-align:right">&nbsp;</td>
          <td width="123" class='taxes' style="text-align:left"><?php echo formatMoney($total['received']); ?></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align:right"><span class="light">Paid</span></td>
          <td style="text-align:left">&nbsp;</td>
          <td class='taxes' style="text-align:left"><?php echo formatMoney($total['paid']); ?></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align:right">Lend</td>
          <td style="text-align:left">&nbsp;</td>
          <td class='taxes' style="text-align:left"><?php echo formatMoney($total['lend']); ?></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align:right">Lend <- Received</td>
          <td style="text-align:left">&nbsp;</td>
          <td class='taxes' style="text-align:left"><?php echo formatMoney($total['lend Received']); ?></td>
        </tr>                        
        <tr>
          <td colspan="3" style="text-align:right">Borrowed</td>
          <td style="text-align:left">&nbsp;</td>
          <td class='taxes' style="text-align:left"><?php echo formatMoney($total['borrowed']); ?></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align:right">Borrowed -> Paid</td>
          <td style="text-align:left">&nbsp;</td>
          <td class='taxes' style="text-align:left"><?php echo formatMoney($total['borrowed Paid']); ?></td>
        </tr>
    </tbody>
</table>
                    </div>
                </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
</body>
</html>