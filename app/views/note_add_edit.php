<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
if(isset($action) == 'edit' && isset($doid)){
$dataNot = $this->note_modl->getNoteDetail($doid);
}
//test($dataNot);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>
			<?php if($action == 'edit'){ ?>
				Note Edit
            <?php } else { ?>
            	Note Add
            <?php } ?>
</title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">

<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#valNotTtl").focus();
	});
</script>
<?php if($action != 'edit'){ ?> 
<script type='text/javascript'>
$(window).load(function(){
$("#valNotTtl").keyup(function(){
   $("#valNotDes").val($("#valNotTtl").val());
});
});
</script>
<?php } ?>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>

<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<?php if(isset($action) == 'edit' && isset($doid)){ ?>
        	<i class="icon-cogs"></i> Note Edit
            <?php } else { ?>
            <i class="glyphicon-pen"></i>Note Add
            <?php } ?>
            
		</h3>
		<p align="right"><button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button></p>
	</div>


<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb">
    
    <div class="control-group">
        <label for="valNotTtl" class="control-label">Note Title</label>
        <div class="controls">
            <input type="text" name="valNotTtl" id="valNotTtl" class="input-block-level" data-rule-required="true"  data-rule-minlength="10" value="<?php echo (isset($dataNot['not_title']))? $dataNot['not_title']: NULL; ?>">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valNotDes" class="control-label">Note Description</label>
        <div class="controls">
            <textarea name="valNotDes" id="valNotDes" class="input-block-level"><?php echo (isset($dataNot['not_desc']))? $dataNot['not_desc']: NULL; ?></textarea>
        </div>
    </div>
			<?php if($action == 'edit'){ ?>
    <div class="control-group">
        <label for="alertTime" class="control-label">Status</label>
        <div class="controls">
            <div class="check-demo-col">
                <div class="check-line">
                    <input type="radio" id="c3" value="3" class='icheck-me' name="valStatus" data-skin="minimal" <?php echo (isset($dataNot['not_status']) && $dataNot['not_status'] == 3)? 'checked': NULL; ?>>
                    <label class='inline' for="c3">Cancelled</label>
                </div>
		        <div class="check-line">
                    <input type="radio" id="c2" value="2" class='icheck-me' name="valStatus" data-skin="minimal" <?php echo (isset($dataNot['not_status']) && $dataNot['not_status'] == 2)? 'checked': NULL; ?>>
                    <label class='inline' for="c2"> Completed</label>
                </div>
                <div class="check-line">
                    <input type="radio" id="c1" value="1" class='icheck-me' name="valStatus" data-skin="minimal" <?php echo (isset($dataNot['not_status']) && $dataNot['not_status'] == 1)? 'checked': NULL; ?>>
                    <label class='inline' for="c1">Pending</label>
                </div>
            </div>
        </div>
    </div>
            <?php } ?>

    <div class="form-actions">

<?php if(isset($action) == 'edit' && isset($doid)){ ?>
        <button type="submit" name="action" value="Save" class="btn btn-primary"><i class="icon-save"></i> Save</button>        
<?php } else { ?>
        <button type="submit" name="action" value="Add" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
<?php } ?>
        <button type="button" class="btn btn-small" onClick="history.back(-1);">Cancel</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>