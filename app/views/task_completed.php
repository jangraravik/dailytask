<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Task Completed</title>

<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">

<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>

<script src="<?php echo base_url('script');?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.core.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>

<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>

<script src="<?php echo base_url('script');?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?php echo base_url('script');?>/plugins/bootbox/jquery.bootbox.js"></script>

<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script src="<?php echo base_url('script');?>/application.js"></script>
<script src="<?php echo base_url('script');?>/demonstration.js"></script>
<script src="<?php echo base_url('script');?>/jquery.quicksearch.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#id_search").quicksearch("table tbody tr", {
			noResults: '#noresults',
			stripeRows: ['odd', 'even'],
			loader: 'span.loading'
		});
	});
</script>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3> <i class="icon-thumbs-up"></i> Task Completed </h3>
	</div>
    <p></p>
        <?php include('inc_msgs.php');?> 
	<div class="box-content nopadding">
	<form action="#">
			<fieldset>
				<input type="text" placeholder="Search" class="input-block-level" name="search" value="" id="id_search" /> <span class="loading">Searching...</span>
			</fieldset>
	</form>
	</div>
	</div>
</div>

<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content nopadding">
<table id="matrix" class="table table-hover table-nomargin table-colored-header">
	<tbody>
	<tr id="noresults">
		<td colspan="3">No match found!</td>
	</tr>
<?php
$dataTasks = $this->task_modl->getAllTaskListByStatus($this->session->userdata['dataUser']['usr_id'],2);
//test($dataTasks);
foreach($dataTasks as $dataTask){
?>
	<tr>
		<td width="61%"><a href="/task/detail/<?php echo encQryStr($dataTask['tsk_id']); ?>"><?php echo $dataTask['tsk_title']; ?></a></td>
		<td width="19%" class='hidden-350'>
	<div id="txtRoll">
		<span id="txtElasped"><?php echo timeElasped($dataTask['tsk_strt_date_time']); ?></span>
		<span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataTask['tsk_strt_date_time']); ?></span>
	</div>
		
		<?php //echo timeElasped($dataTask['tsk_strt_date_time']); ?></td>
	<td width="20%" class='hidden-480'>
<button class="btn btn-warning" rel="tooltip" data-placement="bottom" data-original-title="Reschedule" onClick="window.open('/task/edit/<?php echo encQryStr($dataTask['tsk_id']); ?>','_self')"><i class="icon-time"></i></button>
<button class="btn btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Delete" onClick="window.open('/task/delete/<?php echo encQryStr($dataTask['tsk_id']); ?>','_self')"><i class="icon-remove"></i></button>
	</td>
	</tr>
<?php } ?>

    </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
