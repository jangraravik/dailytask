<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />

<title>Login</title>
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">

<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script src="<?php echo base_url('script');?>/plugins/validation/jquery.validate.min.js"></script>
</head>

<body class='login'>
<div class="wrapper">
  <h1><a href="/"><img src="<?php echo base_url('image');?>/logo-big.png" alt="dayPlaner" class='retina-ready'>dailyTask</a></h1>
  <div class="login-body">
    <h2>Login to your account</h2>
    <?php echo msgsSet($this->session->flashdata('MSGS_TYPE'),$this->session->flashdata('MSGS_VALUE')); ?>
    <form action="" method='post' class='form-validate' id="login">
      <div class="control-group">
        <div class="email controls">
          <input type="text" name='valEmail' placeholder="Email address" class='input-block-level' data-rule-required="true" data-rule-email="true">
        </div>
      </div>
      <div class="control-group">
        <div class="pw controls">
          <input type="password" name="valPass" placeholder="Password" class='input-block-level' data-rule-required="true" data-rule-minlength="8" data-rule-maxlength="10">
        </div>
      </div>
      <div class="form">
        <input type="submit" name="action" value="Login" class='btn btn-primary'>
      </div>
    </form>
    <div class="forget"> <a href="<?php echo base_url('/user/reset'); ?>"><span>Reset password?</span></a> </div>
  </div>
</div>
</body>
</html>