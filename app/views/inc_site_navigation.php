<div class="container-fluid"> <a href="/dashboard" id="brand">dailyTask</a>
<ul class='main-nav'>
    <li <?php echo ($pagTab === 'dashboard') ? 'class="active"' : NULL; ?>><a href="/dashboard"><span>Dashboard</span> </a></li>
    <li <?php echo ($pagTab === 'task') ? 'class="active"' : NULL; ?>>
        <a href="/task" data-toggle="dropdown" class='dropdown-toggle'><span>Task</span>&nbsp;<span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="/task">Pending</a></li>
            <li><a href="/task/completed">Completed</a></li>
            <li><a href="/task/archive">Archive</a></li>
            <li><a href="/task/cancelled">Cancelled</a></li>
        </ul>
    </li>
    
    <li <?php echo ($pagTab === 'note') ? 'class="active"' : NULL; ?>>
        <a href="/note" data-toggle="dropdown" class='dropdown-toggle'><span>Note</span>&nbsp;<span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="/note">Pending</a></li>
            <li><a href="/note/completed">Completed</a></li>
            <li><a href="/note/cancelled">Cancelled</a></li>
        </ul>
    </li>
  
    <li <?php echo ($pagTab === 'calendar') ? 'class="active"' : NULL; ?>><a href="/calendar"><span>Calendar</span></a></li>
    <li <?php echo ($pagTab === 'wallet') ? 'class="active"' : NULL; ?>><a href="/wallet"><span>Wallet</span></a></li>
</ul>
<div class="user">
  <div class="dropdown"> <a href="#" class='dropdown-toggle' data-toggle="dropdown"><img style="width:27px; height:27px;" src="<?php echo $this->user_modl->getUsrPhoto($this->session->userdata['dataUser']['usr_id']); ?>" alt=""></a>
    <ul class="dropdown-menu pull-right">
      <li> <a href="/dashboard"><?php echo $this->user_modl->getUsrName($this->session->userdata['dataUser']['usr_id']); ?></a> </li>
      <li> <a href="/user/profile">Profile</a> </li>
      <li> <a href="/user/logout">Log out</a> </li>
    </ul>
  </div>
</div>
</div>