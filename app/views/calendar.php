<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$dataTasks = $this->task_modl->getAllTaskByUser($this->session->userdata['dataUser']['usr_id']);
//test($dataTasks);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Task Calendar</title>
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">


<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script>
$(document).ready(function() {
// Calendar

$('.calendar').fullCalendar('addEventSource',[
<?php foreach($dataTasks as $dataTask){ ?>
{
	id: <?php echo $dataTask['tsk_id']; ?>,
	title: '<?php echo echoStr($dataTask['tsk_title']); ?>',
	start: <?php echo $dataTask['tsk_strt_date_time']; ?>,
	//end: 1400027201,			
	allDay : false,
	url: '/task/detail/<?php echo encQryStr($dataTask['tsk_id']); ?>',
	backgroundColor: '<?php echo $this->task_modl->getThemBG($dataTask['color_theme']); ?>',
	textColor: '<?php echo $this->task_modl->getThemFont($dataTask['color_theme']); ?>'
},
<?php } ?>
]);
	
});
</script>
<script src="<?php echo base_url('script');?>/plugins/fullcalendar/fullcalendar.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('style');?>/plugins/fullcalendar/fullcalendar.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/plugins/fullcalendar/fullcalendar.print.css" media="print">
<script src="<?php echo base_url('script');?>/application.js"></script>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
  <div id="main" style="margin: 0px;">
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span12">
          <div class="box">
            <div class="box-title">
              <h3> <i class="icon-calendar"></i> Calendar </h3>
            </div>
            <div class="box-content nopadding">
              <div class="calendar"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>