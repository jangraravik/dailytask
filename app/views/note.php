<?php defined('BASEPATH') OR exit('No direct script access allowed');  //echo $pagTab; exit;?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Note Pending</title>

<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">

<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.core.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/bootbox/jquery.bootbox.js"></script>
<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script src="<?php echo base_url('script');?>/application.js"></script>
<script src="<?php echo base_url('script');?>/demonstration.js"></script>
<script src="<?php echo base_url('script');?>/jquery.quicksearch.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#id_search").quicksearch("table tbody tr", {
			noResults: '#noresults',
			stripeRows: ['odd', 'even'],
			loader: 'span.loading'
		});
	});
</script>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<i class="glyphicon-pen"></i>
			Note Pending
		</h3>
		
        <p align="right"><button class="btn btn-primary" name="action" value="add" onClick="window.open('/note/add','_self');"><i class="icon-plus"></i> Add</button></p>
	</div>
    <p></p>
    <?php include('inc_msgs.php');?> 
	<div class="box-content nopadding">
	<form action="#">
			<fieldset>
				<input type="text" placeholder="Search" class="input-block-level" name="search" value="" id="id_search" /> <span class="loading">Searching...</span>
			</fieldset>
	</form>
	</div>
	</div>
</div>

<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content nopadding">
<table id="matrix" class="table table-hover table-nomargin table-colored-header">
	<tbody>
	<tr id="noresults">
		<td colspan="3">No match found!</td>
	</tr>
<?php
$dataNots = $this->note_modl->getAllNotesByUser($this->session->userdata['dataUser']['usr_id']);
//test($dataNots);exit;
foreach($dataNots as $dataNot){
?>
	<tr>
		<td width="61%"><a href="/note/timeline/<?php echo encQryStr($dataNot['not_id']); ?>"><?php echo $dataNot['not_title']; ?></a></td>
		<td width="19%" class='hidden-350'>
	<div id="txtRoll">
		<span id="txtElasped"><?php echo timeElasped($dataNot['not_added_on']); ?></span>
		<span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataNot['not_added_on']); ?></span>
	</div>
		
		<?php //echo timeElasped($dataNot['not_strt_date_time']); ?></td>
	<td width="20%" class='hidden-480'>
<button class="btn btn-success" rel="tooltip" data-placement="bottom" data-original-title="Completed" onClick="window.open('/note/completed/<?php echo encQryStr($dataNot['not_id']); ?>','_self')"><i class="glyphicon-thumbs_up"></i></button>
<button class="btn btn-success" rel="tooltip" data-placement="bottom" data-original-title="Extand" onClick="window.open('/note/extend/<?php echo encQryStr($dataNot['not_id']); ?>','_self');"><i class="glyphicon-chat"></i></button>
<button class="btn btn-warning" rel="tooltip" data-placement="bottom" data-original-title="Edit" onclick="window.open('/note/edit/<?php echo encQryStr($dataNot['not_id']); ?>','_self');"><i class="icon-edit"></i></button>
<button class="btn btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Delete" onclick="window.open('/note/deleted/<?php echo encQryStr($dataNot['not_id']); ?>','_self');"><i class="icon-remove"></i></button>
<button class="btn btn-info" rel="tooltip" data-placement="bottom" data-original-title="Cancelled" onClick="window.open('/note/cancelled/<?php echo encQryStr($dataNot['not_id']); ?>','_self')"><i class="glyphicon-thumbs_down"></i></button>
	</td>
	</tr>
<?php } ?>

    </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
