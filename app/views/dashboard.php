<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$dataUserInfo = $this->user_modl->user_info($this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID']);
$this->session->set_userdata('dataUser',$dataUserInfo);
//test($this->session->all_userdata());exit;


$this->task_modl->makeArchive();

list($dStart,$dEnd) = thisDay();
list($wStart,$wEnd) = thisWeek();
list($mStart,$mEnd) = thisMonth();
list($nmStart,$nmEnd) = nextMonth();
//echo date("M, d Y h:i:s A",$dStart); exit;

$dataDayTasks = $this->task_modl->getAllTaskListByDateInterval(time(),$dEnd,$this->session->userdata['dataUser']['usr_id']);
$dataWeekTasks = $this->task_modl->getAllTaskListByDateInterval($wStart,$wEnd,$this->session->userdata['dataUser']['usr_id']);
$dataMonthTasks = $this->task_modl->getAllTaskListByDateInterval($mStart,$mEnd,$this->session->userdata['dataUser']['usr_id']);
$dataNextMonthTasks = $this->task_modl->getAllTaskListByDateInterval($nmStart,$nmEnd,$this->session->userdata['dataUser']['usr_id']);
//echo test($dataWeekTasks);exit;


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Dashboard</title>
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">

<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script src="<?php echo base_url('script');?>/application.js"></script>
<script src="<?php echo base_url('script');?>/demonstration.js"></script>

</head>
<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="box">
          <div class="box-title">
            <h3> <i class="glyphicon-dashboard"></i> <?php echo $this->user_modl->getUsrName($this->session->userdata['userLogedIn']['THIS_USER_LOGED_ID']); ?>'s Dashboard</h3>
            <div class="pull-right">
						<ul class="stats">
							<li>
								<div class="details">
								You Last logged in <?php echo timeElasped($this->session->userdata['dataUser']['usr_lastlogin']); ?>
								</div>
							</li>
						</ul>
					</div>
          </div>
					
          <p></p>
<?php include('inc_msgs.php');?>       
          
          <div class="box-content">
            <ul class="tiles tiles-center tiles-small">
              <li class="satgreen">
              <a href="/task/add"><span><i class="glyphicon-pushpin"></i></span><span class='name'>Add Task</span></a>
              </li>
                      
              <li class="red">
              <span class="label label-warning">
              <?php echo count($this->task_modl->getAllTaskListByStatus($this->session->userdata['dataUser']['usr_id'],0))." Task"; ?>
              </span> <a href="/task/archive"><span><i class="glyphicon-alarm"></i></span><span class='name'>Archive</span></a>
              </li>

              <li class="orange">
              <span class="label label-info">
              <?php echo count($this->task_modl->getAllTaskListByStatus($this->session->userdata['dataUser']['usr_id'],1))." Task"; ?>
              </span> <a href="task"><span><i class="glyphicon-alarm"></i></span><span class='name'>Pending</span></a>
              </li>
			  
              <li class="blue"> <a href="calendar"><span><i class="glyphicon-calendar"></i></span><span class='name'>Calendar</span></a></li>
              <li class="black"> <span class="label label-inverse">
              <?php echo count($this->note_modl->getAllNotesListByStatus($this->session->userdata['dataUser']['usr_id'],1))." Notes"; ?>
              </span> <a href="note"><span><i class="glyphicon-pen"></i></span><span class='name'>Note</span></a></li>
              <li class="brown"> <a href="wallet"><span><i class="glyphicon-wallet"></i></span><span class='name'>Wallet</span></a></li>
              <li class="pink"> <a href="/user/profile"><span><i class="glyphicon-user"></i></span><span class='name'>Profile</span></a></li>
              <li class="teal"> <a href="/user/settings"><span><i class="glyphicon-settings"></i></span><span class='name'>Settings</span></a></li>
              <li class="lightgrey"> <a href="/user/logout"><span><i class="glyphicon-lock"></i></span><span class='name'>Log Out</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="box">
          <div class="box-title">
            <h3> <i class="icon-bell"></i> Task Notifications </h3>
            <div class="actions"> <a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a> <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a> </div>
          </div>
          <div class="box-content">
            <div class="accordion accordion-widget" id="accordion3">
              <div class="accordion-group red">
                <div class="accordion-heading">
                 	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#c1"> Today's <?php //echo date(MY_DATE_FORMAT,$dStart)." - ".date(MY_DATE_FORMAT,$dEnd); ?><span class="label label-inverse"><?php echo count($dataDayTasks); ?></span> </a>
                 </div>
                <div id="c1" class="accordion-body collapse in">
                  <div class="accordion-inner">
                    <div class="box-content nopadding">
<table class="table table-hover table-nomargin table-condensed">
<?php 
if(count($dataDayTasks) > 0){
foreach($dataDayTasks as $dataDayTask){
?>
<tr>
<td width="70%"><a href="/task/detail/<?php echo encQryStr($dataDayTask['tsk_id']); ?>"><?php echo $dataDayTask['tsk_title']; ?></a></td>
<td width="11%" class='hidden-480'><div id="txtRoll"> <span id="txtElasped"><?php echo timeElasped($dataDayTask['tsk_strt_date_time']); ?></span> <span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataDayTask['tsk_strt_date_time']); ?></span></div></td>
<td width="19%" class='hidden-480' style="text-align:right">
<button class="btn btn-success" rel="tooltip" data-placement="bottom" data-original-title="Completed" onClick="window.open('/task/completed/<?php echo encQryStr($dataDayTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_up"></i></button>
<button class="btn btn-warning" rel="tooltip" data-placement="bottom" data-original-title="Adjourn" onClick="window.open('/task/edit/<?php echo encQryStr($dataDayTask['tsk_id']); ?>','_self')"><i class="icon-time"></i></button>
<button class="btn btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Delete" onClick="window.open('/task/delete/<?php echo encQryStr($dataDayTask['tsk_id']); ?>','_self')"><i class="icon-remove"></i></button>
<button class="btn btn-info" rel="tooltip" data-placement="bottom" data-original-title="Cancelled" onClick="window.open('/task/cancelled/<?php echo encQryStr($dataDayTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_down"></i></button>
</td>
</tr>
<?php } } else { echo 'No Task Pending';} ?>
</table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-group green">
                <div class="accordion-heading">
                	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#c2"> This Week <?php //echo date(MY_DATE_FORMAT,$wStart)." - ".date(MY_DATE_FORMAT,$wEnd); ?><span class="label label-inverse"><?php echo count($dataWeekTasks); ?></span> </a>
                </div>
                <div id="c2" class="accordion-body collapse">
                  <div class="accordion-inner">
<table class="table table-hover table-nomargin table-condensed">
<?php 
if(count($dataWeekTasks) > 0){
foreach($dataWeekTasks as $dataWeekTask){
?>
<tr>
<td width="70%"><a href="task/detail/<?php echo encQryStr($dataWeekTask['tsk_id']); ?>"><?php echo $dataWeekTask['tsk_title']; ?></a></td>
<td width="11%" class='hidden-480'><div id="txtRoll"> <span id="txtElasped"><?php echo timeElasped($dataWeekTask['tsk_strt_date_time']); ?></span> <span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataWeekTask['tsk_strt_date_time']); ?></span></div></td>
<td width="19%" class='hidden-480' style="text-align:right">
<button class="btn btn-success" rel="tooltip" data-placement="bottom" data-original-title="Completed" onClick="window.open('/task/completed/<?php echo encQryStr($dataWeekTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_up"></i></button>
<button class="btn btn-warning" rel="tooltip" data-placement="bottom" data-original-title="Adjourn" onClick="window.open('/task/edit/<?php echo encQryStr($dataWeekTask['tsk_id']); ?>','_self')"><i class="icon-time"></i></button>
<button class="btn btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Delete" onClick="window.open('/task/delete/<?php echo encQryStr($dataWeekTask['tsk_id']); ?>','_self')"><i class="icon-remove"></i></button>
<button class="btn btn-info" rel="tooltip" data-placement="bottom" data-original-title="Cancelled" onClick="window.open('/task/cancelled/<?php echo encQryStr($dataWeekTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_down"></i></button>
</td>
</tr>
<?php } } else { echo 'No Task Pending';} ?>
</table>
                  </div>
                </div>
              </div>
              <div class="accordion-group blue">
                <div class="accordion-heading">
                	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#c3"> This Month <?php //echo date(MY_DATE_FORMAT,$mStart)." - ".date(MY_DATE_FORMAT,$mEnd); ?><span class="label label-inverse"><?php echo count($dataMonthTasks); ?></span> </a>
				</div>
                <div id="c3" class="accordion-body collapse">
                  <div class="accordion-inner">
<table class="table table-hover table-nomargin table-condensed">
<?php 
if(count($dataMonthTasks) > 0){
foreach($dataMonthTasks as $dataMonthTask){
?>
<tr>
<td width="70%"><a href="task/detail/<?php echo encQryStr($dataMonthTask['tsk_id']); ?>"><?php echo $dataMonthTask['tsk_title']; ?></a></td>
<td width="11%" class='hidden-480'><div id="txtRoll"> <span id="txtElasped"><?php echo timeElasped($dataMonthTask['tsk_strt_date_time']); ?></span> <span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataMonthTask['tsk_strt_date_time']); ?></span></div></td>
<td width="19%" class='hidden-480' style="text-align:right">
<button class="btn btn-success" rel="tooltip" data-placement="bottom" data-original-title="Completed" onClick="window.open('/task/completed/<?php echo encQryStr($dataMonthTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_up"></i></button>
<button class="btn btn-warning" rel="tooltip" data-placement="bottom" data-original-title="Adjourn" onClick="window.open('/task/edit/<?php echo encQryStr($dataMonthTask['tsk_id']); ?>','_self')"><i class="icon-time"></i></button>
<button class="btn btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Delete" onClick="window.open('/task/delete/<?php echo encQryStr($dataMonthTask['tsk_id']); ?>','_self')"><i class="icon-remove"></i></button>
<button class="btn btn-info" rel="tooltip" data-placement="bottom" data-original-title="Cancelled" onClick="window.open('/task/cancelled/<?php echo encQryStr($dataMonthTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_down"></i></button>
</td>
</tr>
<?php } } else { echo 'No Task Pending';} ?>
</table>
                  </div>
                </div>
              </div>
              <div class="accordion-group orange">
                <div class="accordion-heading">
                	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#c4"> Next Month <?php //echo date(MY_DATE_FORMAT,$nmStart)." - ".date(MY_DATE_FORMAT,$nmEnd); ?> <span class="label label-inverse"><?php echo count($dataNextMonthTasks); ?></span> </a>
				</div>
                <div id="c4" class="accordion-body collapse">
                  <div class="accordion-inner">
<table class="table table-hover table-nomargin table-condensed">
<?php 
if(count($dataNextMonthTasks) > 0){
foreach($dataNextMonthTasks as $dataNextMonthTask){
?>
<tr>
<td width="70%"><a href="task/detail/<?php echo encQryStr($dataNextMonthTask['tsk_id']); ?>"><?php echo $dataNextMonthTask['tsk_title']; ?></a></td>
<td width="11%" class='hidden-480'><div id="txtRoll"> <span id="txtElasped"><?php echo timeElasped($dataNextMonthTask['tsk_strt_date_time']); ?></span> <span id="txtDateTime"><?php echo date(MY_DATE_FORMAT,$dataNextMonthTask['tsk_strt_date_time']); ?></span></div></td>
<td width="19%" class='hidden-480' style="text-align:right">
<button class="btn btn-success" rel="tooltip" data-placement="bottom" data-original-title="Completed" onClick="window.open('/task/completed/<?php echo encQryStr($dataNextMonthTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_up"></i></button>
<button class="btn btn-warning" rel="tooltip" data-placement="bottom" data-original-title="Adjourn" onClick="window.open('/task/edit/<?php echo encQryStr($dataNextMonthTask['tsk_id']); ?>','_self')"><i class="icon-time"></i></button>
<button class="btn btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Delete" onClick="window.open('/task/delete/<?php echo encQryStr($dataNextMonthTask['tsk_id']); ?>','_self')"><i class="icon-remove"></i></button>
<button class="btn btn-info" rel="tooltip" data-placement="bottom" data-original-title="Cancelled" onClick="window.open('/task/cancelled/<?php echo encQryStr($dataNextMonthTask['tsk_id']); ?>','_self')"><i class="glyphicon-thumbs_down"></i></button>
</td>
</tr>
<?php } } else { echo 'No Task Pending';} ?>
</table> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
