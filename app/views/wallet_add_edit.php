<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
if(isset($action) == 'edit' && isset($doid)){
$dataTrns = $this->wallet_modl->getWltTranDetail($this->session->userdata['dataUser']['usr_id'],$doid);
}
//test($dataTrns);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>
			<?php if($action == 'edit'){ ?>
				Wallet Edit Transaction
            <?php } else { ?>
            	Wallet Add Transaction
            <?php } ?>
</title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">

<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>
<?php if($action != 'edit'){ ?> 
<script type='text/javascript'>
$(window).load(function(){
$("#valWltTtl").keyup(function(){
   $("#valWltDes").val($("#valWltTtl").val());
});
});
</script>
<?php } ?>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>

<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<?php if(isset($action) == 'edit' && isset($doid)){ ?>
        	<i class="glyphicon-wallet"></i> Wallet Edit Transaction
            <?php } else { ?>
            <i class="glyphicon-wallet"></i> Wallet Add Transaction
            <?php } ?>
            
		</h3>
		<p align="right"><button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button></p>
	</div>

<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb">
    
    <div class="control-group">
        <label for="valWltTtl" class="control-label">Transaction Title</label>
        <div class="controls">
            <input type="text" name="valWltTtl" id="valWltTtl" class="input-block-level" data-rule-required="true"  data-rule-minlength="10" value="<?php echo (isset($dataTrns['wlt_trns_title']))? $dataTrns['wlt_trns_title']: NULL; ?>">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valWltDes" class="control-label">Transaction Description</label>
        <div class="controls">
            <textarea name="valWltDes" id="valWltDes" class="input-block-level"><?php echo (isset($dataTrns['wlt_trns_desc']))? $dataTrns['wlt_trns_desc']: NULL; ?></textarea>
        </div>
    </div>
    
    <div class="control-group">
        <label for="valWltDate" class="control-label">Transaction Date</label>
        <div class="controls">
            <input type="text" name="valWltDate" id="valWltDate" class="input-xlarge datepick" readonly data-rule-required="true" value="<?php echo (isset($dataTrns['wlt_trns_date']))? date("d M Y",$dataTrns['wlt_trns_date']): NULL; ?>">
    <script type="text/javascript">
<?php if($action == 'edit'){ ?>
$('#valWltDate').datepicker({
//	format: 'dd-mm-yyyy',
//	format: 'dd/mm/yyyy',
format: 'dd M yyyy',
//	startDate: date,
//endDate: '+0d',
autoclose: true
});
<?php }else{ ?>
$('#valWltDate').datepicker({
//	format: 'dd-mm-yyyy',
//	format: 'dd/mm/yyyy',
format: 'dd M yyyy',
//	startDate: date,
//endDate: '+0d',
autoclose: true
});
<?php } ?>
    </script>                                            
        </div>
    </div>
    
    <div class="control-group">
        <label for="valWltTim" class="control-label">Transaction Time</label>
        <div class="controls">
            <div class="bootstrap-timepicker">
                <input type="text" name="valWltTim" id="valWltTim" class="input-xlarge timepick" value="<?php echo (isset($dataTrns['wlt_trns_date']))? date("H:i A",$dataTrns['wlt_trns_date']): NULL; ?>">
    <script type="text/javascript">
    $('#valWltTim').timepicker({
    defaultTime:'09:00 AM',
    //defaultTime:false,
    minuteStep: 5,
    showInputs: false,
    disableFocus: true,
    showMeridian: true // 12 hrs
    });
    </script>                                                
            </div>
        </div>
    </div>     
    


<div class="control-group">
	<label for="valWltTranAmount" class="control-label">Transaction Amount</label>
	<div class="controls">
		<input type="text" name="valWltTranAmount" id="valWltTranAmount" class="input-xlarge" data-rule-required="true" data-rule-number="true" data-rule-minlength="1" data-rule-maxlength="10" value="<?php echo (isset($dataTrns['wlt_trns_amount']))? $dataTrns['wlt_trns_amount']: NULL; ?>">
	</div>
</div>
									
<div class="control-group">
<label for="valWltTranType" class="control-label">Transaction Type</label>
<div class="controls">
	<select name="valWltTranType" data-rule-required="true" id="tranType" class='input-xlarge-select'>
		<option value=""> - Select -</option>
		<option<?php echo(isset($dataTrns['wlt_trns_type']) && $dataTrns['wlt_trns_type'] == 'received') ? " selected ":NULL; ?> value="received">Received</option>
		<option<?php echo(isset($dataTrns['wlt_trns_type']) && $dataTrns['wlt_trns_type'] == 'paid') ? " selected ":NULL; ?> value="paid">Paid</option>
		<option<?php echo(isset($dataTrns['wlt_trns_type']) && $dataTrns['wlt_trns_type'] == 'lend') ? " selected ":NULL; ?> value="lend">Lend</option>
		<option<?php echo(isset($dataTrns['wlt_trns_type']) && $dataTrns['wlt_trns_type'] == 'lend Received') ? " selected ":NULL; ?> value="lend Received">Lend <- Received</option>
		<option<?php echo(isset($dataTrns['wlt_trns_type']) && $dataTrns['wlt_trns_type'] == 'borrowed') ? " selected ":NULL; ?> value="borrowed">Borrowed</option>
		<option<?php echo(isset($dataTrns['wlt_trns_type']) && $dataTrns['wlt_trns_type'] == 'borrowed Paid') ? " selected ":NULL; ?> value="borrowed Paid">Borrowed -> Paid</option>
	</select>
</div>
</div>




    <div class="form-actions">

<?php if(isset($action) == 'edit' && isset($doid)){ ?>
        <button type="submit" name="action" value="Save" class="btn btn-primary"><i class="icon-save"></i> Save</button>        
<?php } else { ?>
        <button type="submit" name="action" value="Add" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
<?php } ?>
        <button type="button" class="btn btn-small" onClick="history.back(-1);">Cancel</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>