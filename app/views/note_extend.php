<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
//echo $action;exit;
$dataNot = $this->note_modl->getNoteDetail($noteid);
if(isset($action) && $action == 'edit' && isset($doid)){
	$dataTimelime = $this->note_modl->getNoteTimelineDetail($doid);
}
//test($dataTimelime);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>
			<?php if($action == 'edit'){ ?>
				Note Timeline Edit
            <?php } else { ?>
            	Note Timeline Extent
            <?php } ?>
</title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">

<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>

<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<?php if(isset($action)  && $action == 'edit' && isset($doid)){ ?>
        	<i class="icon-comments-alt"></i> Note Extent
            <?php } else { ?>
            <i class="icon-comments-alt"></i> Note Edit
            <?php } ?>
            
		</h3>
		<p align="right"><button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button></p>
	</div>


<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb">
    
    <div class="control-group">
        <label for="valNotTtl" class="control-label">Note Title</label>
        <div class="controls">
            <input type="text" name="valNotTtl" id="valNotTtl" class="input-block-level" disabled value="<?php echo (isset($dataNot['not_title']))? $dataNot['not_title']: NULL; ?>">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valNotDes" class="control-label">Extent Description</label>
        <div class="controls">
            <textarea name="valNotDes" id="valNotDes" data-rule-required="true" class="input-block-level"><?php echo (isset($dataTimelime['not_timlin_comnt']))? $dataTimelime['not_timlin_comnt']: NULL; ?></textarea>
        </div>
    </div>
    
    <div class="control-group">
        <label for="valTskDate" class="control-label">Extent Date</label>
        <div class="controls">
            <input type="text" name="valNotDate" id="valNotDate" class="input-xlarge datepick" readonly data-rule-required="true" value="<?php echo (isset($dataTimelime['not_timlin_datetime']))? date("d M Y",$dataTimelime['not_timlin_datetime']): NULL; ?>">
    <script type="text/javascript">
		$('#valNotDate').datepicker({
		//	format: 'dd-mm-yyyy',
		//	format: 'dd/mm/yyyy',
		format: 'dd M yyyy',
		//	startDate: date,
		//endDate: '+0d',
		autoclose: true
		});   
    </script>                                            
        </div>
    </div>
    
    <div class="control-group">
        <label for="valTskTim" class="control-label">Extent Time</label>
        <div class="controls">
            <div class="bootstrap-timepicker">
                <input type="text" name="valNotTim" id="valNotTim" class="input-xlarge timepick" data-rule-required="true" value="<?php echo (isset($dataTimelime['not_timlin_datetime']))? date("H:i A",$dataTimelime['not_timlin_datetime']): NULL; ?>">
    <script type="text/javascript">
    $('#valNotTim').timepicker({
    //defaultTime:'09:00 AM',
    defaultTime:false,
    minuteStep: 5,
    showInputs: false,
    disableFocus: true,
    showMeridian: true // 12 hrs
    });
    </script>                                                
            </div>
        </div>
    </div>

    <div class="form-actions">
<?php if(isset($action) && $action == 'edit' && isset($doid)){ ?>
        <button type="submit" name="action" value="Save" class="btn btn-primary"><i class="icon-save"></i> Save</button>        
<?php } else { ?>
        <button type="submit" name="action" value="Add" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
<?php } ?>
        <button type="button" class="btn btn-small" onClick="history.back(-1);">Cancel</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>