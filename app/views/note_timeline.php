<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$dataNot = $this->note_modl->getNoteDetail($doid);
$dataTimelimes = $this->note_modl->getAllTimelimeByNote($doid);
//test($dataTimelimes);exit;

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Note Timeline</title>

<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style');?>/themes.css">

<script src="<?php echo base_url('script');?>/jquery.min.js"></script>
<script src="<?php echo base_url('script');?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.core.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('script');?>/plugins/bootbox/jquery.bootbox.js"></script>
<script src="<?php echo base_url('script');?>/app_config.js"></script>
<script src="<?php echo base_url('script');?>/application.js"></script>
<script src="<?php echo base_url('script');?>/demonstration.js"></script>
<script src="<?php echo base_url('script');?>/jquery.quicksearch.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#id_search").quicksearch("table tbody tr", {
			noResults: '#noresults',
			stripeRows: ['odd', 'even'],
			loader: 'span.loading'
		});
	});
</script>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<i class="glyphicon-pen"></i>
			Note Timeline
		</h3>
		
<p align="right">
	<button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button>
</p>
	</div>
    <p></p>
	<div class="box-content nopadding">
	<form action="#">
			<fieldset>
				<input type="text" placeholder="Search" class="input-block-level" name="search" value="" id="id_search" /> <span class="loading">Searching...</span>
			</fieldset>
	</form>
<div class="box-title">
    <h3><i class="icon-comments"></i><?php echo $dataNot['not_title']; ?></h3>
</div>
<ul class="timeline">
<li id="noresults">
    <div class="timeline-content">
            <p>
                No Result Found
            </p>
    <div class="line"></div>


</li>
<?php 
if($dataTimelimes >0){
foreach($dataTimelimes as $dataTimelime){ ?>
<li>
    <div class="timeline-content">
        <div class="left">
            <div class="icon green">
                <i class="icon-comments-alt"></i>
            </div>
			<div class="remove">
			<button class="btn btn-mini btn-blue" rel="tooltip" data-placement="bottom" data-original-title="Edit" onClick="window.open('/note/extend/edit/<?php echo encQryStr($doid); ?>/<?php echo encQryStr($dataTimelime['not_timlin_id']); ?>','_self')"><i class="icon-edit"></i></button>
			<button class="btn btn-mini btn-red" rel="tooltip" data-placement="bottom" data-original-title="Delete" onClick="window.open('/note/extend/delete/<?php echo encQryStr($doid); ?>/<?php echo encQryStr($dataTimelime['not_timlin_id']); ?>','_self')"><i class="icon-remove"></i></button>
			</div>
            <div class="date"><?php echo date("d F, y",$dataTimelime['not_timlin_datetime']); ?></div>
        </div>
        <div class="activity">
            <p>
                <?php echo $dataTimelime['not_timlin_comnt']; ?>
            </p>
        </div>
    </div>
    <div class="line"></div>
</li>
<?php }} ?>

<li>
    <div class="timeline-content">
        <div class="left">
            <div class="icon green">
                <i class="icon-comments-alt"></i>
            </div>
        </div>
        <div class="activity">
            <p>
                <button class="btn btn-primary" name="action" value="add" onClick="window.open('/note/extend/add/<?php echo encQryStr($doid); ?>/<?php echo (isset($dataTimelime['not_timlin_id']))? encQryStr($dataTimelime['not_timlin_id']) : NULL; ?>','_self');"><i class="icon-plus"></i> Extend</button>
            </p>
        </div>
    </div>
    <div class="line"></div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
