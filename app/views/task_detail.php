<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$dataTask = $this->task_modl->getTaskDetail($doid);
//test($dataTask);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>Task Detail <?php //echo $doid;?></title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">

<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>

</head>
<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>
<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<i class="icon-bullhorn"></i>
			Task Detail
		</h3>
		<p align="right"><button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button></p>
	</div>
<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb">

<dl class='dl-horizontal'>
    <dt>Task Title</dt>
    <dd>
        <?php echo $dataTask['tsk_title']; ?>
    </dd>
    <dt>Task Description</dt>
    <dd>
        <?php echo $dataTask['tsk_desc']; ?>
    </dd>
    <dt>Time Elasped</dt>
    <dd>
        <?php echo timeElasped($dataTask['tsk_strt_date_time']); ?>
    </dd>
    <dt>Date / Time</dt>
    <dd>
        <?php echo date(MY_DATE_FORMAT,$dataTask['tsk_strt_date_time']); ?>
    </dd>  
    <dt>Added On</dt>
    <dd>
        <?php echo date(MY_DATE_FORMAT,$dataTask['tsk_added_on']); ?>
    </dd>
    <dt>Task Status</dt>
    <dd>
        <?php echo tskStatus($dataTask['tsk_status']); ?>
    </dd>               
</dl>
    <div class="form-actions">
	
        <button type="button" name="action" class="btn btn-primary" onClick="window.open('/task/edit/<?php echo encQryStr($dataTask['tsk_id']); ?>','_self')"><i class="icon-edit"></i> Edit</button>
		<!--<button type="button" name="action" class="btn btn-red" onClick="window.open('/task/delete/<?php //echo encQryStr($dataTask['tsk_id']); ?>','_self')"><i class="icon-remove"></i> Delete</button> -->
        <button type="button" class="btn btn-small" onClick="history.back(-1);">Cancel</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
