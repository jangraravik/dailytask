<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
if(isset($action) == 'edit' && isset($doid)){
$dataTask = $this->task_modl->getTaskDetail($doid);
}
//test($dataTask);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?php echo base_url('image');?>/favicon.ico" />
<title>
			<?php if($action == 'edit'){ ?>
				Task Edit
            <?php } else { ?>
            	Task Add
            <?php } ?>
</title>
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/style.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/themes.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/datepicker/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url('style'); ?>/plugins/icheck/all.css">

<!-- SCRIPTS -->
<script src="<?php echo base_url('script'); ?>/jquery.min.js"></script>
<script src="<?php echo base_url('script'); ?>/jquery.form.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/validation/additional-methods.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('script'); ?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/icheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('script'); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url('script'); ?>/app_config.js"></script>
<script src="<?php echo base_url('script'); ?>/application.js"></script>
<script src="<?php echo base_url('script'); ?>/demonstration.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#valTaskTtl").focus();
	});
</script>
<?php if($action != 'edit'){ ?> 
<script type='text/javascript'>
$(window).load(function(){
$("#valTaskTtl").keyup(function(){
   $("#valTaskDes").val($("#valTaskTtl").val());
});
});
</script>
<?php } ?>
</head>

<body>
<div id="navigation">
<?php include('inc_site_navigation.php');?>
</div>

<div class="container-fluid" id="content">
<div id="main" style="margin: 0px;">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="box">
	<div class="box-title">
		<h3>
			<?php if(isset($action) == 'edit' && isset($doid)){ ?>
        	<i class="icon-cogs"></i> Task Edit
            <?php } else { ?>
            <i class="icon-pushpin"></i>Task Add
            <?php } ?>
            
		</h3>
		<p align="right"><button class="btn btn-primary" name="action" value="add" onclick="history.back(-1);"><i class="icon-chevron-left"></i> Back</button></p>
	</div>


<div class="box-content">
<form action="" method="post" class='form-horizontal form-validate' id="bb">
    
    <div class="control-group">
        <label for="valTaskTtl" class="control-label">Task Title</label>
        <div class="controls">
            <input type="text" name="valTaskTtl" id="valTaskTtl" class="input-block-level" data-rule-required="true"  data-rule-minlength="10" value="<?php echo (isset($dataTask['tsk_title']))? $dataTask['tsk_title']: NULL; ?>">
        </div>
    </div>
    
    <div class="control-group">
        <label for="valTaskDes" class="control-label">Task Description</label>
        <div class="controls">
            <textarea name="valTaskDes" id="valTaskDes" class="input-block-level"><?php echo (isset($dataTask['tsk_desc']))? $dataTask['tsk_desc']: NULL; ?></textarea>
        </div>
    </div>
    
    <div class="control-group">
        <label for="valTskDate" class="control-label">Task Date</label>
        <div class="controls">
            <input type="text" name="valTskDate" id="valTskDate" class="input-xlarge datepick" readonly data-rule-required="true" value="<?php echo (isset($dataTask['tsk_strt_date_time']))? date("d M Y",$dataTask['tsk_strt_date_time']): NULL; ?>">
    <script type="text/javascript">
		$('#valTskDate').datepicker({
		//	format: 'dd-mm-yyyy',
		//	format: 'dd/mm/yyyy',
		format: 'dd M yyyy',
		//	startDate: date,
		//endDate: '+0d',
		autoclose: true
		});  
    </script>                                            
        </div>
    </div>
    
    <div class="control-group">
        <label for="valTskTim" class="control-label">Task Time</label>
        <div class="controls">
            <div class="bootstrap-timepicker">
                <input type="text" name="valTskTim" id="valTskTim" class="input-xlarge timepick" data-rule-required="true" value="<?php echo (isset($dataTask['tsk_strt_date_time']))? date("H:i A",$dataTask['tsk_strt_date_time']): NULL; ?>">
    <script type="text/javascript">
    $('#valTskTim').timepicker({
    //defaultTime:'09:00 AM',
    defaultTime:false,
    minuteStep: 5,
    showInputs: false,
    disableFocus: true,
    showMeridian: true // 12 hrs
    });
    </script>                                                
            </div>
        </div>
    </div>     
    
    <div class="control-group">
        <label for="valTskClrThem" class="control-label">Task Highlighter</label>
        <div class="controls">
            <select name="valTskClrThem" id="valTskClrThem" class='input-xlarge-select'>   
		        <option class="thmeAqua" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 1)? 'selected': NULL; ?> value="1">Aqua</option>
		        <option class="thmeAquaDark" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 2)? 'selected': NULL; ?> value="2">Aqua Dark</option>
		        <option class="thmeBlack" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 5)? 'selected': NULL; ?> value="5">Black</option>
		        <option class="thmeBlue" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 3)? 'selected': NULL; ?> value="3">Blue</option>
		        <option class="thmeBlueDark" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 4)? 'selected': NULL; ?> value="4">Blue Dark</option>
		        <option class="thmeGreen" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 6)? 'selected': NULL; ?> value="6">Green</option>
		        <option class="thmeOrange" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 7)? 'selected': NULL; ?> value="7">Orange</option>
		        <option class="thmePink" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 8)? 'selected': NULL; ?> value="8">Pink</option>
		        <option class="thmePinkDark" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 9)? 'selected': NULL; ?> value="9">Pink Dark</option>                                                                                                                                
		        <option class="thmePurple" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 10)? 'selected': NULL; ?> value="10">Purple</option>
		        <option class="thmePurpleDark" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 11)? 'selected': NULL; ?> value="11">Purple Dark</option>
		        <option class="thmeRed" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 12)? 'selected': NULL; ?> value="12">Red</option>
		        <option class="thmeYellow" <?php echo (isset($dataTask['color_theme']) && $dataTask['color_theme'] == 13)? 'selected': NULL; ?> value="13">Yellow</option>                                                                
            </select>
        </div>
    </div>
    

    <div class="control-group">
        <label for="valTskRept" class="control-label">Task Repeat</label>
        <div class="controls">
            <select name="valTskRepTyp" id="valTskRepTyp" style="width:100px;"class='input-small-select'>
                <option value="once">Once</option>                                            
<!--
                <option value="day">Daily</option>
                <option value="week">Weekly</option>
                <option value="month">Monthly</option>
                <option value="year">Yearly</option>												
-->
            </select>                                         
            <select name="valTskReptTim" style="width:50px;" class='input-small-select'>
                <option value="1">1</option>
<!--            
				<option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
-->
            </select>
            time
        </div>
    </div>

			<?php if($action == 'edit'){ ?>
    <div class="control-group">
        <label for="alertTime" class="control-label">Status</label>
        <div class="controls">
            <div class="check-demo-col">
                <div class="check-line">
                    <input type="radio" id="c3" value="3" class='icheck-me' name="valStatus" data-skin="minimal" <?php echo (isset($dataTask['tsk_status']) && $dataTask['tsk_status'] == 3)? 'checked': NULL; ?>>
                    <label class='inline' for="c3">Cancelled</label>
                </div>
		        <div class="check-line">
                    <input type="radio" id="c2" value="2" class='icheck-me' name="valStatus" data-skin="minimal" <?php echo (isset($dataTask['tsk_status']) && $dataTask['tsk_status'] == 2)? 'checked': NULL; ?>>
                    <label class='inline' for="c2"> Completed</label>
                </div>
                <div class="check-line">
                    <input type="radio" id="c1" value="1" class='icheck-me' name="valStatus" data-skin="minimal" <?php echo (isset($dataTask['tsk_status']) && $dataTask['tsk_status'] == 1)? 'checked': NULL; ?>>
                    <label class='inline' for="c1">Pending</label>
                </div>
            </div>
        </div>
    </div>
            <?php } ?>
<!--
    <div class="control-group">
        <label for="alertTime" class="control-label">Alert Me</label>
        <div class="controls">
            <select name="alertTime" id="alertTime" class='input-xlarge-select'>
                <option value="30">before 30 minute</option>
            </select>
        </div>
    </div>
-->    
    <div class="control-group">
    <label for="valTskAlrtVia" class="control-label">Send Alert</label>
        <div class="controls">
            <div class="check-demo-col">
                <div class="check-line">
                    <input type="checkbox" name="valTskAlrtMail" value="1" class='icheck-me' disabled <?php //echo (isset($dataTask['tsk_is_alrt_on_mail']) && $dataTask['tsk_is_alrt_on_mail'] == 1)? 'checked':NULL; ?> id="ymail" data-skin="minimal" >
                    <label class='inline' for="ymail">Email</label>
                </div>
                <div class="check-line">
                    <input type="checkbox" name="valTskAlrtSMS" value="1" class='icheck-me' disabled <?php //echo (isset($dataTask['tsk_is_alrt_on_sms']) && $dataTask['tsk_is_alrt_on_sms'] == 1)? 'checked':NULL; ?> id="ysms" data-skin="minimal" >
                    <label class='inline' for="ysms">SMS</label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">

<?php if(isset($action) == 'edit' && isset($doid)){ ?>
        <button type="submit" name="action" value="Save" class="btn btn-primary"><i class="icon-save"></i> Save</button>      
<?php } else { ?>
        <button type="submit" name="action" value="Add" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
<?php } ?>
        <button type="button" class="btn btn-small" onClick="history.back(-1);">Cancel</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>