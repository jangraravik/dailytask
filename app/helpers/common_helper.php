<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*

if (!function_exists('another_function')){

    function another_function($value){

        // return $value;

    }

}

*/



/*## Array Debug ##*/

if(!function_exists('test')){

  function test($var)
  {
    echo '<pre>';
    if(is_array($var)) {
      print_r($var);
    } else {
      var_dump($var);
    }
    echo '</pre>';
  }

}





## Encode URL Query String ##
if(!function_exists('encQryStr')){
	function encQryStr($valu) {
		return rtrim(strtr(base64_encode($valu), '+/', '-_'), '=');
	}
}

## Decode URL Query String ##
if(!function_exists('decQryStr')){
	function decQryStr($valu) {
		return base64_decode(str_pad(strtr($valu, '-_', '+/'), strlen($valu) % 4, '=', STR_PAD_RIGHT));
	} 
}


/*## SEO Friendly Query Strings ##*/

if(!function_exists('seoFriendlyURL')){

	function seoFriendlyURL($strVlu){

		$strVlu = str_replace(array('[\', \']'), '', $strVlu);

		$strVlu = preg_replace('/\[.*\]/U', '', $strVlu);

		$strVlu = preg_replace('/&(AMp;)?#?[a-z0-9]+;/i', '-', $strVlu);

		$strVlu = htmlentities($strVlu, ENT_COMPAT, 'utf-8');

		$strVlu = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $strVlu );

		$strVlu = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $strVlu);

		$strVlu = str_replace('20', '', $strVlu); 

		return strtolower(trim($strVlu, '-'));

	}

}






## Session Messages and Errors ##
if(!function_exists('msgsSet')){
	function msgsSet($type,$value){
		switch($type){
			case 'success':
			return "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>".$value."</div>";
			break;
			
			case 'error':
			return "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>".$value."</div>";
			break;
			
			case 'info':
			return "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>".$value."</div>";
			break;
					
			case 'alert':
			return "<div class='alert alert-Alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> You are Welcome </div>";
			break;		
		}
	}
}




## Task Status ##
if(!function_exists('tskStatus')){
	function tskStatus($status){
		switch($status){
			case 0:
			return "<span class='label label-important'>Archived</span>";
			break;
			
			case 1:
			return "<span class='label label-warning'>Pending</span>";
			break;
			
			case 2:
			return "<span class='label label-completed'>Completed</span>";
			break;
					
			case 3:
			return "<span class='label label-info'>Cancelled</span>";
			break;		
		}
	}
}


## Session Messages and Errors ##
/*
if(!function_exists('msgs')){
function msgs(){
	if(isset(isset($_SESSION['MSGS_TYPE']){
		return msgsSet($this->session->flashdata('MSGS_TYPE'),$this->session->flashdata('MSGS_VALUE'));
	}
	else{
		
	}
}
}
*/



/*## Filter the String for Special Charectors if any used ##*/

if(!function_exists('escStr')){
	function escStr($string){
		//global $db;
		//$escaped_string = mysqli_real_escape_string($db, $string);
		//return html_entity_decode($string);
		//return html_entity_decode($string);
		return htmlentities($string,ENT_QUOTES,"UTF-8");
	}
}

/*## Filter the String for Special Charectors if any used ##*/

if(!function_exists('echoStr')){
	function echoStr($string){
		//return html_entity_decode($string,ENT_QUOTES,"ISO-8859-1"); #NOTE: UTF-8 does not work!
		//return htmlentities($string, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		return htmlspecialchars($string);
	}
}


## Genrate Secure Code ##
if(!function_exists('rendVerifyCode')){
	function rendVerifyCode($len = 20){
		$randCodresult = "";
		$chars = "abcdefghijklmnopqrstuvwxyz!@#$%&?ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$randCodresult .= "".$charArray[$randItem];
		}
		return $randCodresult;
	}
}




## Backword Time Stemp ##
if(!function_exists('timeBackword')){
function timeBackword($time){
	$curTime	= time();
	$output		= '';
	
	if($time >= $curTime){
		$output = "Invalid Time";
	}else{
	$timeCalculated = $curTime - $time;
	$seconds 	= $timeCalculated ;
	$minutes 	= round($timeCalculated / 60 );
	$hours 		= round($timeCalculated / 3600);
	$days 		= round($timeCalculated / 86400 );
	$weeks 		= round($timeCalculated / 604800);
	$months 	= round($timeCalculated / 2600640 );
	$years 		= round($timeCalculated / 31207680 );
	
		// Seconds
		if($seconds <= 60){
			$output = "$seconds seconds ago";
		}
		//Minutes
		else if($minutes <=60){
			if($minutes==1){
				$output = "one minute ago";
			}
			else{
				$output = "$minutes minutes ago";
			}
		}
		//Hours
		else if($hours <=24){
			if($hours==1){
				$output = "an hour ago";
			}else{
				$output = "$hours hours ago";
			}
		}
		//Days
		else if($days <= 7){
			if($days==1){
				$output = "yesterday";
			}else{
				$output = "$days days ago";
			}
		}
		//Weeks
		else if($weeks <= 4.3){
			if($weeks==1){
				$output = "1 week ago";
			}else{
				$output = "$weeks weeks ago";
			}
		}
		//Months
		else if($months <=12){
			if($months==1){
				$output = "1 month ago";
			}else{
				$output = "$months months ago";
			}
		}
		//Years
		else{
			if($years==1){
				$output = "1 year ago";
			}else{
				$output = "$years years ago";
			}
		}
	}
	return $output;	
}
}




## Forward Time Stemp ##
if(!function_exists('timeForward')){
function timeForward($time){
	$curTime = time();
	$output = '';
	if($time <= $curTime){
		$output = "Invalid Time";
	}else{
		$timeCalculated = $time - $curTime;
		$seconds 	= $timeCalculated ;
		$minutes 	= round($timeCalculated / 60 );
		$hours 		= round($timeCalculated / 3600);
		$days 		= round($timeCalculated / 86400 );
		$weeks 		= round($timeCalculated / 604800);
		$months 	= round($timeCalculated / 2600640 );
		$years 		= round($timeCalculated / 31207680 );
		
		// Seconds
		if($seconds <= 60){
			$output = "$seconds seconds left";
		}
		//Minutes
		else if($minutes <=60){
			if($minutes==1){
				$output = "1 minute left";
			}
			else{
				$output = "$minutes minutes left";
			}
		}
		//Hours
		else if($hours <=24){
			if($hours==1){
				$output = "1 hour left";
			}else{
				$output = "$hours hours left";
			}
		}
		//Days
		else if($days <= 7){
			if($days==1){
				$output = "tomorrow";
			}else{
				$output = "$days days left";
			}
		}
		//Weeks
		else if($weeks <= 4.3){
			if($weeks==1){
				$output = "1 week left";
			}else{
				$output = "$weeks weeks left";
			}
		}
		//Months
		else if($months <=12){
			if($months==1){
				$output = "1 month left";
			}else{
				$output = "$months months left";
			}
		}
		//Years
		else{
			if($years==1){
				$output = "1 year left";
			}else{
				$output = "$years years left";
			}
		}
	}
	return $output;
}
}



## Elasped Time Stemp ##
if(!function_exists('timeElasped')){
	function timeElasped($time){
		$curTime = time();
		if($time <= $curTime){
			return timeBackword($time);
		}else{
			return timeForward($time);
		}
	}
}



## Today Length AM to PM ##
if(!function_exists('thisDay')){
	function thisDay(){
		$dayStart	= strtotime("today 12:00:00 AM");
		$dayEnd		= strtotime("today 11:59:00 PM");
		return array($dayStart,$dayEnd);
	}
}

## Current Week Length First Day(excluding dead days so counting only remaining days) to Last Day wo the week ##
if(!function_exists('thisWeek')){
	function thisWeek(){
		$weekStart	= strtotime("today +1 day 12:00:00 AM"); // Excluding past days of the week from Current Week
		//$weekStart	= strtotime("monday this week 12:00:00 AM"); // Default is
		$weekEnd	= strtotime("this sunday 11:59:00 PM");
		return array($weekStart,$weekEnd);
	}
}


## Current Month Length First Day(excluding dead days so counting only remaining days) to Last Day wo the Month ##
if(!function_exists('thisMonth')){
	function thisMonth(){
		$monthStart	= strtotime("this sunday 12:00:00 PM"); //Start remaining month from the end of this week.
		//$monthStart	= strtotime("today +1 day 12:00:00 AM"); // Excluding past days of the month from Current month
		//$monthStart	= strtotime("first day of this month 12:00:00 AM"); // Default is
		$monthEnd	= strtotime("last day of this month 11:59:00 PM");
		return array($monthStart,$monthEnd);
	}
}

## Next Month Length First Date to Last date of the Next Month ##
if(!function_exists('nextMonth')){
	function nextMonth(){
		/*
		// Excluding Current Week 7 days
		$now = time(); // or your date as well
		$nxtMnthSundy = strtotime("this sunday 11:59:00 PM");	
		$datediff = $now - $nxtMnthSundy;
		$days = floor($datediff/(60*60*24));
		
		if($days > 1){
			$monthStart	= strtotime("this sunday 11:59:00 PM");			
		}else{
			$monthStart	= strtotime("first day of next month 12:00:00 AM");
		}
		*/
		$monthStart	= strtotime("first day of next month 12:00:00 AM");
		$monthEnd	= strtotime("last day of next month 11:59:00 PM");
		return array($monthStart,$monthEnd);
	}
}
## Next Month Length First Date to Last date of the Next Month ##
if(!function_exists('anyMonth')){
	function anyMonth($month){
		$monthStart	= strtotime("first day of $month"); // Default is
		$monthEnd	= strtotime("last day of $month");
		return array($monthStart,$monthEnd);
	}
}

## Repeat the task on daily(+n day), weekly(+n week), monthly(+n month),annually(+n year) basis ##
if(!function_exists('taskRepetition')){
	function taskRepetition($date,$notim,$reptyp){
		$setDats = array();
		$i=1;
		while($i<=$notim){
			$setDats[] = $date;
			$date = strtotime("+1 $reptyp", $date);
			$i++;
		}
		return $setDats;
	}
}

## Numbers to Money Format. ##
if(!function_exists('formatMoney')){ 
function formatMoney($number, $fractional=true) {
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
} 
}