<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_modl extends CI_Model {

	public function __construct(){
        parent::__construct();
    }
		
	
	public function register($data){
		$this->db->insert('user',$data);
		return $this->db->insert_id();
	}
	
	public function update($data,$uid){
		$this->db->where('usr_id',$uid);
		$this->db->update('user',$data);
		return true;
	}
	
	public function user_info($uid){
		$this->db->select('usr_id,usr_lastlogin');
		$this->db->where('usr_id',$uid);
		$query = $this->db->get('user');
		return $query->row_array();
	}

	public function user_info_all($uid){
		$this->db->select('usr_id,usr_name,usr_email,usr_mobile,usr_dob,usr_gender,usr_about,usr_place_city,usr_lastlogin');
		$this->db->where('usr_id',$uid);
		$query = $this->db->get('user');
		return $query->row_array();
	}

	
	public function getUsrPhoto($uid){
		$this->db->select('usr_photo');
		$this->db->where('usr_id',$uid);
		$query = $this->db->get('user');
		$data = $query->row_array();
		return $data['usr_photo'];
	}
	
	public function getUsrName($uid){
		$this->db->select('usr_name');
		$this->db->where('usr_id',$uid);
		$query = $this->db->get('user');
		$data = $query->row_array();
		return $data['usr_name'];
	}

	
	public function validate_user($email,$pass){
		//$this->db->select('usr_id');
		$this->db->where('usr_email',$email);
		$this->db->where('usr_pass',$pass);
		$this->db->where('usr_isactive',1);
		$query = $this->db->get("user");
		//echo $query->num_rows();exit; /* Found the Row */
        
        if($query->num_rows() == 1){

			$dataRow= $query->row_array();
			//test($dataRow);exit;
			//echo $dataRow['usr_id'];exit;
			$make_user_sess_array = array(
						'THIS_USER_LOGED_IN' => 'ONLINE',
						'THIS_USER_LOGED_ID' => $dataRow['usr_id']
			 );
			 
         	$this->session->set_userdata('userLogedIn',$make_user_sess_array);
			//test($this->session->all_userdata());exit;
			return true;
		}
		return false;
    }
	
	
	public function validate_user_email($email){
		$this->db->where('usr_email',$email);
		$this->db->where('usr_isactive',1);
		$query=$this->db->get("user");
        
        if($query->num_rows()>0){
        	return true;
		}
		return false;
    }


	public function validate_user_email_verify_code($email,$code){
		$this->db->where('usr_email',$email);
		$this->db->where('usr_verify_code',$code);		
		$this->db->where('usr_isactive',1);
		$query=$this->db->get("user");
        
        if($query->num_rows()>0){
        	return true;
		}
		return false;
    }
	
	public function validate_user_password($pass,$uid){
		$this->db->where('usr_id',$uid);
		$this->db->where('usr_pass = ',$pass);		
		$this->db->where('usr_isactive',1);
		$query=$this->db->get("user");
        
        if($query->num_rows()>0){
        	return true;
		}
		return false;
    }
		
	public function set_new_password($updateData,$codeID){
		$this->db->where('usr_verify_code', $codeID);
		$this->db->update('user', $updateData);
		return true;
	}
}