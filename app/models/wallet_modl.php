<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_modl extends CI_Model {

	public function __construct(){
        parent::__construct();
    }
	
	public function create($data){
		$this->db->insert('wallet',$data);
		return $this->db->insert_id();
	}
		
	public function update($data,$uid,$doid){
		$this->db->where('usr_id',$uid);
		$this->db->where('wlt_trns_id',$doid);
		$this->db->update('wallet',$data);
		return true;
	}
	
	public function detete($doid){
		$this->db->where('tsk_id',$doid);
		$this->db->delete('task');
		return true;
	}


//////////////////////* Additional Functions *//////////////////////

	public function makeArchive(){
		$data = array(
			'tsk_status' => 0
        );
		$this->db->where('tsk_status',1);
		$this->db->where('tsk_strt_date_time <= ',time());
		$this->db->update('task',$data);
		return true;
	}
		
	public function getWltTranDetail($uid,$id){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('wlt_trns_id',$id);		
		$query = $this->db->get('wallet');
		return $query->row_array();
	}
	
	public function getAllWltTranByUser($uid,$datStrt,$datEnd){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('wlt_trns_date >=',$datStrt);
		$this->db->where('wlt_trns_date <=',$datEnd);
		$this->db->order_by("wlt_trns_date", "ASC");		
		$query = $this->db->get('wallet');
		return $query->result_array();
	}

}