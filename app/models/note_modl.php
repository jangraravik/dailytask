<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Note_modl extends CI_Model {

	public function __construct(){
        parent::__construct();
    }
	
	public function create($data){
		$this->db->insert('note',$data);
		return $this->db->insert_id();
	}
	
	public function create_timeline($data){
		$this->db->insert('note_timeline',$data);
		return $this->db->insert_id();
	}
		
	public function update_timeline($data,$doid){
		$this->db->where('not_timlin_id',$doid);
		$this->db->update('note_timeline',$data);
		return true;
	}
	
	public function update($data,$doid){
		$this->db->where('not_id',$doid);
		$this->db->update('note',$data);
		return true;
	}
	
	public function detete($doid){
		$this->db->where('not_id',$doid);
		$this->db->delete('note');
		return true;
	}
	
	public function detete_timeline($doid){
		$this->db->where('not_timlin_id',$doid);
		$this->db->delete('note_timeline');
		return true;
	}


//////////////////////* Additional Functions *//////////////////////


	public function getAllNotesByUser($uid){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('not_status',1);
		$query = $this->db->get('note');
		return $query->result_array();
	}

	public function getAllNotesListByStatus($uid,$status){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('not_status',$status);
		$this->db->order_by("not_added_on", "DESC");
		$query = $this->db->get('note');
		return $query->result_array();
	}
	
	public function getNoteDetail($id){
		$this->db->select('*');
		$this->db->where('not_id',$id);
		$query = $this->db->get('note');
		return $query->row_array();
	}

	public function getAllTimelimeByNote($id){
		$this->db->select('*');
		$this->db->where('not_id',$id);
		$this->db->order_by("not_timlin_datetime", "ASC");
		$query = $this->db->get('note_timeline');
		return $query->result_array();
	}
	
	public function getNoteTimelineDetail($id){
		$this->db->select('*');
		$this->db->where('not_timlin_id',$id);
		$query = $this->db->get('note_timeline');
		return $query->row_array();
	}
	
}