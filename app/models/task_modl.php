<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task_modl extends CI_Model {

	public function __construct(){
        parent::__construct();
    }
	
	public function create($data){
		$this->db->insert('task',$data);
		return $this->db->insert_id();
	}
		
	public function update($data,$doid){
		$this->db->where('tsk_id',$doid);
		$this->db->update('task',$data);
		return true;
	}
	
	public function detete($doid){
		$this->db->where('tsk_id',$doid);
		$this->db->delete('task');
		return true;
	}


//////////////////////* Additional Functions *//////////////////////

	public function makeArchive(){
		$data = array(
			'tsk_status' => 0
        );
		$this->db->where('tsk_status',1);
		$this->db->where('tsk_strt_date_time <= ',time());
		$this->db->update('task',$data);
		return true;
	}
		
	public function getTaskDetail($id){
		$this->db->select('*');
		$this->db->where('tsk_id',$id);
		$query = $this->db->get('task');
		return $query->row_array();
	}
	
	public function getAllTaskByUser($uid){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$query = $this->db->get('task');
		return $query->result_array();
	}
	
	public function getAllTaskArchived($uid){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('tsk_strt_date_time <= ',time());
		$this->db->where('tsk_status',0);		
		$this->db->order_by("tsk_strt_date_time", "ASC");
		$query = $this->db->get('task');
		return $query->result_array();
	}
	

	public function getAllTaskListByStatus($uid,$status){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('tsk_status',$status);
		$this->db->order_by("tsk_strt_date_time", "ASC");
		$query = $this->db->get('task');
		return $query->result_array();
	}

	public function getAllTaskListByDateInterval($timStart,$timEnd,$uid){
		$this->db->select('*');
		$this->db->where('usr_id',$uid);
		$this->db->where('tsk_strt_date_time >=',$timStart);
		$this->db->where('tsk_strt_date_time <=',$timEnd);
		$this->db->where('tsk_status',1);
		$this->db->order_by("tsk_strt_date_time", "ASC");
		$query = $this->db->get('task');
		return $query->result_array();
	}

	public function getAllThemes(){
			$this->db->select('*');
			$this->db->where('them_id !=',0);
			$this->db->order_by("them_name", "ASC");		
			$query = $this->db->get('task_color_theme');
			return $query->result_array();
	}

	public function getThemFont($id){
		$this->db->select('them_font');
		$this->db->where('them_id',$id);
		$query = $this->db->get('task_color_theme');
		return $query->row('them_font');
	}
	
	public function getThemBG($id){
		$this->db->select('them_bg');
		$this->db->where('them_id',$id);
		$query = $this->db->get('task_color_theme');
		return $query->row('them_bg');
	}
}