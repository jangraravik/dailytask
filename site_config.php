<?php

##########################################################################
## Server Values  							##
##########################################################################
define("SITE_ENVIRONMENT",'Development');
//define("SITE_ENVIRONMENT",'Production');


switch(SITE_ENVIRONMENT){

/*  Development Server Values  */
	case 'Development':
		define("THIS_DB_HOST",'localhost');
		define("THIS_DB_USER",'root');
		define("THIS_DB_PASS",'root');
		define("THIS_DB_NAME",'dailytask');
	break;
	
/*  Production Server Values */
	case 'Production':
		define("THIS_DB_HOST",'localhost');
		define("THIS_DB_USER",'');
		define("THIS_DB_PASS",'');
		define("THIS_DB_NAME",'');
	break;
	
	default:
		exit('The application SERVER TYPE is not set correctly.');
	break;
}